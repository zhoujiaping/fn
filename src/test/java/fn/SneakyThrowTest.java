package fn;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SneakyThrowTest {
    interface F1<R,P1>{
        R apply(P1 p1) throws Throwable;
        @SneakyThrows
        default R sneakyApply(P1 p1){
            return apply(p1);
        }
    }
    @Test
    public void testSneakyThrowsWithLambda(){
        System.out.println(invoke((String p)->{
            Class.forName("java.lang.String");
            return "hello "+p;
        },"zhou"));
        Assertions.assertDoesNotThrow(()->new RuntimeException());
    }
    private <R,P1> R invoke(F1<R,P1> fn,P1 p1){
        return fn.sneakyApply(p1);
    }
}
