package fn;

import lombok.Lombok;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ErrorsTest {
    /**
    * sneaky函数，包裹一段代码，将checked exception去掉。
     * 主要用于Consumer、Supplier等不接收checked exception的函数。
    * */
    @Test
    public void testSneaky(){
       try{
           Class.forName("fn.a.b.c.Whatever");
       }catch (Exception e){
           Assertions.assertEquals(ClassNotFoundException.class,e.getClass());
       }
    }

    @Test
    public void testRethrow(){
        /**
         * rethrow函数，将异常重新抛出去，不转换异常类型。但是可以绕过编译器对checked exception的检查。
         * 通常我们用下面的方式，会改变异常类型。
         * try{
         *     ...
         * }catch(Exception e){
         *     throw new RuntimeException(e);
         * }
         * */
        try {
            try {
                Class.forName("fn.a.b.c.Whatever");
            } catch (Exception e) {
                throw Lombok.sneakyThrow(e);
            }
        }catch (Exception e){
            Assertions.assertEquals(ClassNotFoundException.class,e.getClass());
        }
    }

}
