package fn;

import fn.tco.TailRecur;
import fn.tco.Tco3;
import org.junit.jupiter.api.Test;
import org.springframework.util.StopWatch;
import org.testng.Assert;

public class Tco3Test {
    @Test
    public void testTcoPerform() throws Exception {

        Class<? extends Tco3Test> clazz = Tco3.tco(Tco3Test.class);
        Tco3Test o = clazz.newInstance();
        StopWatch sw = new StopWatch();

        sw.start("直接调用");
        add(1, 10000);
        sw.stop();
        sw.start("尾递归优化（预热）");
        o.add(1,3);
        sw.stop();
        sw.start("尾递归优化");
        int ans = o.add(1, 10000);
        sw.stop();
        System.out.println(ans);
        for (StopWatch.TaskInfo it : sw.getTaskInfo()) {
            System.out.println(it.getTaskName() + "=>" + it.getTimeNanos()+"(ns),"+it.getTimeMillis()+"(ms)");
        }
        //打印结果：
        //10001
        //直接调用=>1306400(ns),1(ms)
        //尾递归优化（预热）=>97853100(ns),97(ms)
        //尾递归优化=>11804600(ns),11(ms)

    }
    @Test
    public void testTco() throws Exception {
        Class<? extends Tco3Test> clazz = Tco3.tco(Tco3Test.class);
        Tco3Test o = clazz.newInstance();
        int ans = o.add(1, 10000);
        Assert.assertEquals(ans,10001);
    }
    //只能对成员方法进行尾递归优化，不能对静态方法进行尾递归优化。
    //因为静态方法调用不支持多态。
    @TailRecur
    protected int add(int x, int y) {
        if (y > 0) {
            return add(x + 1, y - 1);
        }
        return x;
    }
}
