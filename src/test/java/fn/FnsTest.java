package fn;

import fn.model.User;
import lombok.Lombok;
import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.BiFunction;

import static fn.Colls.arrayList;
import static fn.Fns.*;
import static fn.IOs.*;
import static fn.Mores.*;

public class FnsTest {
    @Test
    public void testDefn() {
        /*
        如果没有defn，比如像下面这样，是无法通过编译的。因为编译器无法推断f到底是什么类型。
        val f = (User user, Integer ageAdd)->{
            user.setAge(user.getAge()+ageAdd);
            return user;
        };
        使用defn之后，代码可以简洁一些。
        特别是如果需要定义一个多参（大于2个）的函数，没有defn我们必须先定义一个函数式接口。
        */
        val fn = fn((User user, Integer ageAdd) -> {
            user.setAge(user.getAge() + ageAdd);
            return user;
        });
        val user = fn.apply(let(new User(), it -> it.setAge(5)), 10);
        println(user);
        Assertions.assertEquals(15, user.getAge());
    }

    @Test
    public void testFunctionException() {
        /**
         * the exception type has been replaced.
         * */
        Assertions.assertThrows(RuntimeException.class, () -> arrayList(".").forEach(it -> {
            try {
                Files.readAllLines(Paths.get(it), StandardCharsets.UTF_8);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }));
        /**
         * the exception is original exception.
         * */
        Assertions.assertThrows(IOException.class, () -> {
            arrayList(".").forEach(it -> {
                        try {
                            Files.readAllLines(Paths.get(it), StandardCharsets.UTF_8);
                        } catch (IOException e) {
                            throw Lombok.sneakyThrow(e);
                        }
                    }
            );
        });
    }
}
