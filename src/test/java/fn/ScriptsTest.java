package fn;

import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static fn.Scripts.*;
import static fn.Colls.*;
public class ScriptsTest {

    @Test
    public void testScripts(){
        var obj = evalMvel("a.b[0]",hashMap("a", treeMap("b",arrayList("hello"))));
        Assertions.assertEquals("hello",obj);
    }
}
