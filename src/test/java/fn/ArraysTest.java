package fn;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static fn.Arrays.*;
import static fn.Arrays.array;
import static fn.Colls.arrayList;

public class ArraysTest {
    @Test
    public void testArrayOf() {
        Integer[] intArr = array( 1, 2, 3);
        Assertions.assertArrayEquals(intArr, new Integer[]{1, 2, 3});

        val strArr = array("hello", "world");
        Assertions.assertArrayEquals(strArr, new String[]{"hello", "world"});
    }

    @Test
    public void testNewArray() {
        String[] arr = newArray(String.class, 10);
        Assertions.assertArrayEquals(arr, new String[10]);
    }

    @Test
    public void testEachWithIndex4Array() {
        List<String> list = arrayList();
        each(array(String.class, "one", "two"), (i, it) -> {
            list.add(i + "=>" + it);
        });
        Assertions.assertTrue(list.size() == 2);
        Assertions.assertEquals(list.get(0), "0=>one");
        Assertions.assertEquals(list.get(1), "1=>two");
    }
}
