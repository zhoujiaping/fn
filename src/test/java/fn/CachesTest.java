package fn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.function.Supplier;

import static fn.Caches.memoize;
import static fn.Colls.linkedList;

class CachesTest {
    @Test
    public void testMemoize() {
        //lazy init，cache
        LinkedList<String> list = linkedList("a");
        Supplier<String> supplier = memoize(() -> {
            String value = list.getFirst();
            list.addFirst("b");
            return value;
        });
        String value = supplier.get();
        Assertions.assertEquals("a",value);
        Assertions.assertEquals( "a",supplier.get());
        Assertions.assertEquals("a",supplier.get());
    }

}
