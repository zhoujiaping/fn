package fn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static fn.IOs.printf;
import static fn.IOs.println;

public class IOsTest {
    @Test
    public void testPrintln() {
        println("hello world");
    }

    @Test
    public void testPrintf() {
        /**
         * System.out#printf will throw exception when the arguments is not enough.
         * but guava Strings#lenientFormat is ok.
         * */
        printf("%s,%s", 1, 2, 3);
        printf("%s,%s,%s", 1, 2);//ok
        Assertions.assertThrows(Exception.class, () -> {
            System.out.printf("%s,%s,%s", 1, 2);//error
        });
    }
}
