package fn;

import fn.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import static fn.Colls.*;
import static fn.Mores.*;

public class MoresTest {
    @Test
    public void testLetAndAlso() {
        //code is separated at different block, it makes read easy.
        List<String> list1 = let(arrayList("hello"), it -> {
            it.add("one");
            it.add("two");
            it.add("three");
        });
        Assertions.assertEquals(list1, arrayList("hello", "one", "two", "three"));

        List<String> list2 = also(arrayList("HELLO"), it -> {
            it.add("one");
            it.add("two");
            it.add("three");
            return it;
        });
        Assertions.assertEquals(list2, arrayList("HELLO", "one", "two", "three"));

        User user = let(new User(), it -> {
            it.setName("zhou");
            it.setAge(18);
        });

        Assertions.assertEquals(user.getName(), "zhou");
        Assertions.assertEquals(user.getAge(), 18);

    }
}
