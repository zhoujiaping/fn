//package fn;
//
//import fn.model.Dog;
//import fn.model.User;
//import fn.tuple.Tuple2;
//import lombok.val;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//
//import static fn.Mores.let;
//import static fn.utils.Reflects.*;
//import static fn.IOs.*;
//import static fn.Colls.*;
//import static fn.Arrays.*;
//
//public class ReflectsTest {
//    /**
//     * 不管它的构造函数如何定义，统统可以创建。
//     */
//    @Test
//    public void testForceNew() {
//        Dog dog = forceNew(Dog.class);
//        Assertions.assertNull(dog.getName());
//    }
//
//    @Test
//    public void testForceNewTuple() {
//        val tuple2 = forceNew(Tuple2.class);
//        println(tuple2._1);
//        println(tuple2._2);
//    }
//
//    @Test
//    public void testEachFields() {
//        Dog dog = new Dog("neo");
//        eachFields(Dog.class, f -> println(f.getName()), f -> true);
//    }
//
//    /**
//     * 通过表达式获取统一的getter和setter。
//     * 对于数组和List，使用[idx]的下表语法。
//     * 对于Map，使用['key']的语法。
//     * 对于其他对象，使用.fieldName语法。
//     * 为什么不统一map和其他对象？因为如果map里面有个size的key，那么不知道该返回什么了。（到底是键值对里面有名字为size的key，还是map的size属性？）
//     */
//    @Test
//    public void testFieldValue() {
//        val data = arrayList(
//                hashMap("a", array(Dog.class,
//                        new Dog("neo")
//                ))
//        );
//        val fieldVal = fieldValue(data, "[0]['a'][0].name");
//        Assertions.assertEquals("neo", fieldVal.get());
//        fieldVal.set("NEO");
//        Assertions.assertEquals("NEO", fieldVal.get());
//    }
//
//    @Test
//    public void testFieldValue2() {
//        /**
//         *
//         * */
//        val list = arrayList(
//                hashMap("my.friends", arrayList(
//                        let(new User(), it -> {
//                            it.setName("jay");
//                        }),
//                        let(new User(), it -> it.setName("chou"))
//                ))
//        );
//        //index support array,list.
//        //key support object,map.
//        //field name support object.
//        val name1 = fieldValue(list, "[0]['my.friends'][1]['name']").get();
//        fieldValue(list, "[0]['my.friends'][1].name").set("neo");
//        val name2 = fieldValue(list, "[0]['my.friends'][1].name").get();
//        Assertions.assertEquals(name1, "chou");
//        Assertions.assertEquals(name2, "neo");
//
//    }
//
//    @Test
//    public void testInvokeMethod() {
//        /**spring's invokeMethod，we need findMethod before.
//         * but findMethod match the method name, and exactly argument types.
//         *
//         * what if i don't know the argument types?
//         * even if we can call getClass, and what if the argument value is null?
//         *
//         * and what if the actual argument type is the subclass of virtual argument type.
//         *
//         * we need a method, like responseTo in groovy lang.
//         */
//        //no arguments
//        val methods = responseTo(this.getClass(), "testToDate");
//        methods.forEach(it -> println(invokeMethod(it, this)));
//        //actual argument is null
//        Object[] args = new Object[]{1};
//        val methods2 = responseTo(this.getClass(), "hello", args);
//        methods2.forEach(it -> println(invokeMethod(it, this, args)));
//    }
//
//    private String hello(int name) {
//        return "hello " + name;
//    }
//
//    private String hello(String name) {
//        return "hello " + name;
//    }
//
//    private String hello(String name, String name2) {
//        return "hello " + name;
//    }
//}
