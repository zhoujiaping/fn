package fn;


import fn.st.STM;
import fn.st.StatefulSTM;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static fn.Colls.linkedHashMap;

public class StateMachineTest {
    LinkedHashMap<String, Map<String, String>> doorStateTransfers = linkedHashMap(
            "opened", linkedHashMap(
                    "close", "closed"
            ),
            "closed", linkedHashMap(
                    "open", "opened",
                    "lock", "locked"
            ),
            "locked", linkedHashMap(
                    "unlock", "closed"
            )
    );

    @Test
    public void testSTM1() {
        String st = StatefulSTM.defStm(doorStateTransfers).init("closed").transfer("open", (oldState, newState) -> {
            System.out.println(oldState + "=>" + newState);
        },e->{
            System.out.println(e.getMessage());
        }).getState();
        System.out.println(st);
    }

    @Test
    public void testSTM2() {
        String st = StatefulSTM.defStm(doorStateTransfers, "closed").transfer("unlock", (oldState, newState) -> {
            System.out.println(oldState + "=>" + newState);
        }).getState();
        System.out.println(st);
    }

    @Test
    public void testSTM3() {
        STM.defStm(doorStateTransfers).transfer("locked", "unlock", (oldState, newState) -> {
            System.out.println(oldState + "=>" + newState);
        },e->{
            System.out.println(e.getMessage());
        });
    }
}
