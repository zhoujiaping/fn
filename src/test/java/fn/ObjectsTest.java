package fn;

import fn.model.Dog;
import fn.model.Person;
import fn.model.User;
import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static fn.Colls.*;
import static fn.IOs.println;
import static fn.Arrays.*;
import static fn.Mores.let;
import static fn.Objects.*;

public class ObjectsTest {
    @Test
    public void testAsBool() {
        val values1 = arrayList("", null, 0, 0.0, 0.0f, new BigDecimal("0.00"), new BigInteger("0"), false, newArray(String.class, 0), linkedList(), '\0', '\u0000', hashMap());
        println(values1.stream().map(it -> asBoolean(it)).collect(Collectors.toList()));
        Assertions.assertTrue(values1.stream().allMatch(it -> !asBoolean(it)));

        val values2 = arrayList(" ", new Object(), 1, 0.1, 0.2f, true, Optional.ofNullable(null), newArray(String.class, 1), arrayList(0), ' ', treeMap(1, 2));
        println(values2.stream().map(it -> asBoolean(it)).collect(Collectors.toList()));
        Assertions.assertTrue(values2.stream().allMatch(it -> asBoolean(it)));

    }

    @Test
    public void testOrElse4String() {
        String str1 = orElse(null, "ok");
        Assertions.assertEquals(str1, "ok");
        String str2 = orElse("a", "ok");
        Assertions.assertEquals(str2, "a");
    }

    @Test
    public void testInstanceOf() {
        Assertions.assertTrue( isInstanceOf("",CharSequence.class));
    }

    @Test
    public void testCast() {
        //the 1st edition
        List<List<String>> listOfList0 = new ArrayList<>();
        List<String> list = new ArrayList<>();
        list.add("hello");
        listOfList0.add(list);
        Object obj1 = listOfList0;
        List<List<String>> listOfList1 = (List<List<String>>) obj1;
        System.out.println(listOfList1);

        //the 2nd edition
        Object obj2 = arrayList(arrayList("hello"));
        List<List<String>> listOfList2 = cast(obj2);
        println(listOfList2);

        //the 3rd edition
        List<List<String>> listOfList3 = arrayList(arrayList("hello"));
        println(listOfList3);

        //the 4th edition
        println(arrayList(arrayList("hello")));
    }

    @Test
    public void testCopy() {
        //copy a object witch implemented Serializable.
        User user1 = let(new User(), it -> {
            it.setName("zhou");
            it.setAge(18);
        });
        User user2 = deepCopy(user1);
        user2.setAge(20);
        Assertions.assertEquals(user2.getName(), "zhou");
        Assertions.assertEquals(user1.getAge(), 18);
    }

    @Test
    public void testCovariant() {
        //the 1st edition
        val users = arrayList(
                let(new User(), it -> it.setName("puck")),
                let(new User(), it -> it.setName("am"))
        );
        //the 2nd edition
        List<Person> persons = cast(users);
        List<Person> persons2 = (List<Person>) (Object) users;
        //
        println(persons);
        println(persons2);
        //except no exception throws
    }

    @Test
    public void testConvert() {
        Assertions.assertEquals(1, convert("1", Integer.class));
        Assertions.assertEquals(1L, convert("1", Long.class));
        Assertions.assertEquals(1L, convert(BigDecimal.ONE, Long.class));
        Assertions.assertEquals(1L, convert(1, Long.class));
        Assertions.assertEquals("1", convert(1, String.class));
        Assertions.assertEquals(BigDecimal.ONE, convert(1, BigDecimal.class));
    }

    @Test
    public void testToStr(){
        println(Objects.toStr(new Dog("jack")));
    }
}
