package fn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static fn.IOs.*;
import static fn.utils.Resources.*;

public class ResourcesTest {
    @Test
    public void testAsText(){
        String location = "classpath:META-INF/maven/com.google.guava/guava/pom.xml";
        String text = readText(location);
        println(text);
        Assertions.assertTrue(text.contains("guava"));
    }
}
