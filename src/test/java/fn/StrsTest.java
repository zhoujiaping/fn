package fn;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static fn.Strs.*;
import static fn.Colls.*;

public class StrsTest {
    @Test
    public void testStr(){
        Assertions.assertEquals("hello, jack!",str("hello",", jack","!"));
        Assertions.assertEquals("********",repeat("*",8));
        Assertions.assertEquals("hi,jack%s",format("%s%s%s","hi,","jack"));
        Assertions.assertEquals("Asserti...",
                truncate("Assertions.assertEquals",10,"..."));
    }

    @Test
    public void testPadStart() {
        //such as generate a order no.
        Assertions.assertEquals(padStart("1", 6, '0'), "000001");
    }

    @Test
    public void testTruncate() {
        val str = truncate("Apache Groovy is a powerful, optionally typed and dynamic language.",
                20, "...");
        Assertions.assertEquals(str, "Apache Groovy is ...");
    }

    @Test
    public void testFmt(){
        val str = "Apache {} is {} {}";
        val fmtStr = fmt(str,"{","}","Groovy","a");
        Assertions.assertEquals("Apache Groovy is a {}",fmtStr);
    }

    @Test
    public void testFmtIndexed(){
        val str = "Apache {2} is {0} {1}";
        val fmtStr = fmt(str,"{","}","a","powerful");
        Assertions.assertEquals("Apache {2} is a powerful",fmtStr);
    }

    @Test
    public void testFmtMap(){
        val str = "Apache {lang} is {a} {what}";
        val fmtStr = fmtMap(str,"{","}",
                hashMap("lang","groovy","what","powerful"));
        Assertions.assertEquals("Apache groovy is {a} powerful",fmtStr);

    }

    @Test
    public void testFmtMapNullValue(){
        val str = "Apache {{lang}} is {{a}} {{what}}";
        val fmtStr = fmtMap(str,"{{","}}",
                hashMap("lang","groovy","what","powerful","a",null));
        Assertions.assertEquals("Apache groovy is null powerful",fmtStr);
    }

}
