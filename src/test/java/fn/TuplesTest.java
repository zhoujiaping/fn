package fn;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static fn.Colls.arrayList;
import static fn.Tuples.*;

public class TuplesTest {
    @Test
    public void testTuple(){
        val tuple = tuple("jack",20,"doctor");
        Assertions.assertEquals("jack",tuple._1);
        Assertions.assertEquals(20,tuple._2);
        Assertions.assertEquals("doctor",tuple._3);
    }

    @Test
    public void testTuple3() {
        val tuple3 = tuple("zhou", 20, arrayList("a", "b"));
        Assertions.assertEquals(tuple3.toString(), "Tuple3(_1=zhou, _2=20, _3=[a, b])");
    }
}
