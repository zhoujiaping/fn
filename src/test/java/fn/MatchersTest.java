package fn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;
import static fn.Matchers.*;

public class MatchersTest {
    @Test
    public void test(){
        String str = replaceNth("1 abc 2 abc 3 abc", "((a)b)", 1, (idx, s) -> s.substring(0, 1));
        Assertions.assertEquals("1 abc 2 ac 3 abc",str);
        str = replaceFirstN("1 abc 2 abc 3 abc", "((a)b)", 1, (idx, s) -> s.substring(0, 1));
        Assertions.assertEquals("1 ac 2 ac 3 abc",str);
        str = replaceAll("1 abc 2 abc 3 abc", "abc", (idx, s) -> s + "d");
        Assertions.assertEquals("1 abcd 2 abcd 3 abcd",str);
    }
}
