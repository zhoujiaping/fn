package fn;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static fn.Dates.*;
public class DatesTest {
    @Test
    public void testToDate() {
        /**
         * String,Date,LocalDate,LocalDateTime转换
         * */
        val date1 = toDate("2021/06/26 18:30", "yyyy/MM/dd HH:mm");
        val date2 = toDate(toLocalDate("2021/06/26", "yyyy/MM/dd"));
        //基于默认格式，更简洁
        val date3 = toDate("2021-06-26T18:30:21");
        val date4 = toDate(toLocalDateTime("2021/06/26 18:30", "yyyy/MM/dd HH:mm"));

        Assertions.assertEquals(date1, Date.from(LocalDateTime.of(2021, 06, 26, 18, 30).atZone(ZoneId.systemDefault()).toInstant()));
        Assertions.assertEquals(date2, Date.from(LocalDate.of(2021, 06, 26).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        Assertions.assertEquals(date3, Date.from(LocalDateTime.of(2021, 06, 26, 18, 30, 21).atZone(ZoneId.systemDefault()).toInstant()));
        Assertions.assertEquals(date4, Date.from(LocalDateTime.of(2021, 06, 26, 18, 30).atZone(ZoneId.systemDefault()).toInstant()));
    }
}
