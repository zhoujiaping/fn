package fn;

import static fn.Threads.*;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static fn.IOs.*;

public class ThreadsTest {
    @Test
    @SneakyThrows
    public void testSetTimeout() {
        println(new Date());
        setTimeout(1000, () -> println(new Date()));
        Thread.sleep(2000);
    }

    /**
     * 1s后每隔1s打印一次日期。
     * 8s之后，关闭线程池。
     */
    @Test
    @SneakyThrows
    public void testSetInterval() {
        setInterval(1000, 1000, () -> println(new Date()));
        setTimeout(8000, () -> shutdownThreadPool());
        println(submit(() -> "hello world!").get());
        Thread.sleep(12000);
    }
}
