package fn;

import fn.model.User;
import lombok.Lombok;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import static fn.Colls.*;
import static fn.utils.Proxys.*;
import static fn.Strs.*;
import static fn.Fns.*;
import static fn.IOs.*;

public class ProxysTest {
    @SneakyThrows
    @Test
    public void testProxy() {
        List<String> calls = arrayList();
        Class<? extends User> proxyClass = proxy(User.class, (target, method, superMethod, args) -> {
            calls.add(format("%s(%s)", method.getName(), Arrays.toString(args)));
            if (method.getName().equals("getName")) {
                return "JOKER";
            } else if (method.getName().equals("hello")) {
                return "HELLO";
            }
            try {
                return superMethod.invoke(target, args);
            } catch (Exception e) {
                throw Lombok.sneakyThrow(e);
            }
        });
        User user = proxyClass.getConstructor().newInstance();
        user.setName("joker");
        String name = user.getName();
        Assertions.assertEquals("JOKER",name);
        //甚至可以代理静态和final方法。不过代理静态方法后，要通过反射调用，不然调用的还是原来的方法。
        Assertions.assertEquals("hello", user.hello());
        Assertions.assertEquals("HELLO", proxyClass.getDeclaredMethod("hello").invoke(null));
        Assertions.assertEquals("[setName([joker]), getName([]), hello([])]",calls.toString());
    }
}
