package fn;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static fn.Colls.*;

public class CollsTest {
    @Test
    public void testArrayList() {
        val list = arrayList("hello", "java");
        Assertions.assertEquals( 2,list.size());
        Assertions.assertEquals( "hello",list.get(0));
        Assertions.assertEquals( "java",list.get(1));

        List<String> list0 = arrayList();
        Assertions.assertEquals( 0,list0.size());
    }
    @Test
    public void testHashSet() {
        val set = hashSet("hello", "java");
        Assertions.assertEquals( 2,set.size());
        Assertions.assertTrue(set.contains("hello"));
        Assertions.assertTrue(set.contains("java"));
    }

    @Test
    public void testHashMap() {
        Map<Integer, String> map0 = hashMap(1, "ONE",
                2, "TWO",
                3, "THREE"
        );
        Assertions.assertEquals("{1=ONE, 2=TWO, 3=THREE}",map0.toString());

        val map = hashMap("am", 'a',
                "bm", 'b',
                "cm", 'c');
        Assertions.assertEquals( 3,map.size());
        Assertions.assertEquals( 'a',map.get("am"));
        Assertions.assertEquals( 'b',map.get("bm"));
        Assertions.assertEquals( 'c',map.get("cm"));
    }
    @Test
    public void testZipMap() {
        val keys = arrayList("am", "bm", "cm");
        val values = arrayList('a', 'b', 'c');
        val map = zipMap(keys, values);

        Assertions.assertEquals( 3,map.size());
        Assertions.assertEquals( 'a',map.get("am"));
        Assertions.assertEquals( 'b',map.get("bm"));
        Assertions.assertEquals( 'c',map.get("cm"));
    }

    @Test
    public void testUnzipMap() {
        val map = treeMap("am", 'a',
                "bm", 'b',
                "cm", 'c');
        val pairs = unzipMap(map);
        Assertions.assertEquals( arrayList("am", "bm", "cm"),pairs._1);
        Assertions.assertEquals( arrayList('a', 'b', 'c'),pairs._2);
    }


    @Test
    public void testConcat() {
        val list1 = arrayList("A", "B", "C");
        val list2 = arrayList("D", "E");
        val list3 = concat(list1, list2);
        Assertions.assertTrue(list3.size() == 5);
        Assertions.assertEquals( "A",list3.get(0));
        Assertions.assertEquals( "B",list3.get(1));
        Assertions.assertEquals( "C",list3.get(2));
        Assertions.assertEquals( "D",list3.get(3));
        Assertions.assertEquals( "E",list3.get(4));

        Assertions.assertTrue(list1.size() == 3);
        Assertions.assertTrue(list2.size() == 2);
    }

    @Test
    public void testReverse() {
        val list1 = arrayList("A", "B", "C");
        Assertions.assertEquals( arrayList("C", "B", "A"),reverse(list1));
    }

    @Test
    public void testInverse() {
        val list1 = arrayList("A", "B", "C");
        val list2 = arrayList("a", "b", "c");
        val map = zipMap(list1, list2);
        val pairs = unzipMap(inverse(map));
        Assertions.assertEquals( list2,pairs._1);
        Assertions.assertEquals( list1,pairs._2);
    }

    @Test
    public void testSlice() {
        val list = arrayList("1", "2", "3", "4", "5");
        val list2 = slice(list, -4, -2);
        Assertions.assertIterableEquals( arrayList("2", "3", "4"),list2);
        Assertions.assertIterableEquals(  list,slice(list,0, -1));
    }

    @Test
    public void testDiff() {
        val list1 = arrayList("a", "b", "c");
        val set1 = hashSet("a", "c", "d");
        Assertions.assertTrue( diff(list1,set1).containsAll(arrayList("b")));
        Assertions.assertEquals( 1,diff(list1,set1).size());
        Assertions.assertTrue(diff(set1,list1).containsAll(arrayList("d")));
        Assertions.assertEquals(1,diff(set1,list1).size());
    }

    @Test
    public void testIntersect() {
        val list1 = arrayList("a", "b", "c");
        val set1 = hashSet("a", "c", "d");
        Assertions.assertTrue( intersect(list1,set1).containsAll(arrayList("a", "c")));
        Assertions.assertEquals(2,intersect(list1,set1).size());
    }

    @Test
    public void testUnion() {
        val list1 = arrayList("a", "b", "c");
        val set1 = hashSet("a", "c", "d");
        Assertions.assertTrue( union(list1,set1).containsAll(arrayList("a", "c", "d", "b")));
        Assertions.assertEquals(4,union(list1,set1).size());
    }

    @Test
    public void testEachWithIndex4Iter() {
        val list = arrayList("A", "B", "C", "D");
        val result = arrayList();
        each(list, (i, it) -> result.add(i + "=>" + it));
        Assertions.assertIterableEquals( arrayList("0=>A", "1=>B", "2=>C", "3=>D"),result);
    }

}
