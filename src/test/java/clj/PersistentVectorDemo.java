package clj;

import clojure.lang.PersistentVector;

import java.util.ArrayList;
import java.util.List;

public class PersistentVectorDemo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<1024+32;i++){
            list.add(i);
        }
        PersistentVector pv = PersistentVector.create(list);
        System.out.println(pv);
        PersistentVector pv2 = pv.cons(64).cons(65);
        System.out.println(pv2);
    }
}
