package org.sirenia.func.mixin

import fn.FnMixin
import fn.anno.SourceMixin
import javassist.ClassPool
import javassist.CtClass
import javassist.CtMethod
import java.lang.reflect.Modifier

/**
 * unfortunately, we cann't generate source class with generic type!
 */
def generate(File classpath) {
    def sourceClass = FnMixin
    SourceMixin mixin = sourceClass.getAnnotation(SourceMixin)
    def targetClassName = mixin.name()
    ClassPool pool = ClassPool.getDefault()
    try {
        pool.get(targetClassName)
        return
    }catch(e){
        //noop
    }
    CtClass cc = pool.makeClass(targetClassName);

    def classes = mixin.value()
    classes.each {
        clazz ->
            def funcClass = pool.get(clazz.getName())
            funcClass.declaredMethods.each {
                method ->
                    //filter non-public
                    if (!Modifier.isPublic(method.modifiers)) {
                        return
                    }
                    //filter lambda
                    if (method.name.contains('$')) {
                        return
                    }
                    def newMethod = new CtMethod(method,cc,null)
                    cc.addMethod(newMethod)
            }
    }
    cc.writeFile(classpath.absolutePath)
}
def getModuleRoot(){
    def root = new File(System.getProperty("user.dir"))
    while (root.absolutePath.indexOf("src")>0) {
        root = root.parentFile
    }
    println root
    root
}
generate(getModuleRoot())