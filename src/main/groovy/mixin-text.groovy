import fn.FnMixin
import fn.anno.SourceMixin

/**
 * generate mixin java source file.
 * if the source file is well format.
 * it's not perfect, but it will not loss information.
 * */
def generate(File classpath) {
    def sourceClass = FnMixin
    SourceMixin mixin = sourceClass.getAnnotation(SourceMixin)
    def targetClassName = mixin.name()
//        try {
//            Class.forName(targetClassName)
//            return
//        }catch(e){
//            //noop
//        }

    def moduleRoot = getModuleRoot()
    while ('src' in moduleRoot.absolutePath) {
        moduleRoot = moduleRoot.parentFile
    }

    def pkg = targetClassName.split(/\./)[0..-2].join('.')
    def simpleName = targetClassName.split(/\./)[-1]
    def imports = [] as Set
    def body = []
    def classes = mixin.value()
    classes.each {
        clazz ->
            def javaFileName = clazz.name.replaceAll(/\./, '/') + '.java'
            def javaFile = new File(moduleRoot, "/src/main/java/$javaFileName")
            javaFile.readLines().findAll {
                if (it.matches(/^.*static\s+final.*$/)) {
                    imports << "import static ${clazz.name}.*;"
                    return false
                }
                it.trim() && !it.matches(/^\s*package.*$/)
                        && !it.matches(/^.*abstract\s+class.*$/)
                && !it.matches(/^\s*@Slf4j.*$/)
            }.each {
                if (it.startsWith("import")) {
                    imports << it
                } else {
                    body << it
                }
            }
            //the last }
            body[-1] = ''
    }
    def importLines = imports.join('\n')
    def bodyLines = body.join('\n').replaceAll(/(?s)\/\/\s*ignore-mixin-begin.*?\/\/\s*ignore-mixin-end/,'')
    new File(classpath, simpleName + '.java').text = """\
package $pkg;

$importLines

public abstract class ${simpleName}{

$bodyLines

}
"""
}
def getModuleRoot(){
    def root = new File(System.getProperty("user.dir"))
    while (root.absolutePath.indexOf("src")>0) {
        root = root.parentFile
    }
    println root
    root
}
generate(new File(getModuleRoot(), "/src/main/java/fn"))

