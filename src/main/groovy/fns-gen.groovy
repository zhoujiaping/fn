(2..7).each {
    def argNums = it
    def argTypes = (1..it).collect { "P$it" }.join(", ")
    def argDeclares = (1..it).collect { "P$it p$it" }.join(", ")
    def args = (1..it).collect { "p$it" }.join(", ")
    def defaults = (1..it - 1).collect {
        def _restArgTypes = (it + 1..argNums).collect { "P$it" }.join(", ")
        def _argDeclares = (1..it).collect { "P$it p$it" }.join(", ")
        def _restArgDeclares = (it + 1..argNums).collect { "P$it p$it" }.join(", ")
        """
        default Fn${argNums-it}<${_restArgTypes},R> apply(${_argDeclares}) {
            return (${_restArgDeclares}) -> apply(${args});
        }
"""
    }.join("\n")
    def code = """\
    /**
     * ${it}元函数
     */
    public interface Fn${it}<${argTypes},R> {
        R apply(${argDeclares});

${defaults}        
    }
"""
    println code
}