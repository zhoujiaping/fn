(1..7).each {
    def genericTypes = (1..it).collect{
        n->
            "P$n"
    }.join(", ")
    def args = (1..it).collect{
        n->
            "p$n"
    }.join(", ")
    def key = (1..it).collect{
        n->
            "p$n"
    }.join("+ ")
    def code = """\
    public static <$genericTypes,R> Fn$it<$genericTypes,R> threadCaching(String exp, Fn$it<$genericTypes,R> fn){
        String prefix = cacheCounter.getAndIncrement()+"#";
        return ($args)->{
            Map<String,Object> cache = cacheTl.get();
            if(cache == null){
                return fn.apply($args);
            }
            String cacheKey = prefix+"#"+parseExpression(exp, array($args));
            R res = (R) cache.get(cacheKey);
            if(res == null){
                res = fn.apply($args);
                cache.put(cacheKey, res);
            }
            return res;
        };
    }
"""
    println code
}

