(1..7).each{
    def genericTypes = (1..it).collect{
        n->
            "P$n"
    }.join(", ")
    def code = """\
    public static <$genericTypes, R> Fn$it<$genericTypes,R> defn(Fn$it<$genericTypes, R> fn){
        return fn;
    }
"""
    println code
}

