package fn;

import fn.anno.SourceMixin;
import fn.utils.Proxys;
import fn.utils.Reflects;

@SourceMixin(name="fn.Cores",value={Arrays.class,Caches.class, Colls.class,
        Dates.class, Fns.class, IOs.class, Mores.class,
        Objects.class, Strs.class, Threads.class, Tuples.class,Matchers.class})
public abstract class FnMixin {
}
