package fn;

import org.mvel2.MVEL;

public abstract class Scripts {
    //ignore-mixin-begin
    private Scripts(){}
    //ignore-mixin-end
    public static Object evalMvel(String express, Object ctx) {
        return MVEL.eval(express, ctx);
    }
}
