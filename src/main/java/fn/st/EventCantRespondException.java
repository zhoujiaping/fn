package fn.st;

public class EventCantRespondException extends RuntimeException {
    private String state;
    private String event;

    public String getState() {
        return state;
    }

    public String getEvent() {
        return event;
    }

    public EventCantRespondException(String state, String event, String msg) {
        super(msg);
        this.state = state;
        this.event = event;
    }
}
