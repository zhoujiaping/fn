package fn.st;

import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static fn.Colls.treeSet;

/**
 * 用于有状态的状态机。状态由状态机记录。
 */
public class StatefulSTM {
    protected Map<String, Map<String, String>> transfers;
    private String state;

    private StatefulSTM() {
    }

    public StatefulSTM init(String initState) {
        if (!transfers.containsKey(initState)) {
            throw new UndefinedStateException(initState, "state " + initState + " not defined");
        }
        state = initState;
        return this;
    }


    public String getState() {
        return state;
    }

    public static StatefulSTM defStm(Map<String, Map<String, String>> transfers) {
        StatefulSTM stm = new StatefulSTM();
        stm.transfers = transfers;
        return stm;
    }

    public static StatefulSTM defStm(Map<String, Map<String, String>> transfers, String initState) {
        return defStm(transfers).init(initState);
    }

    public StatefulSTM transfer(String evt, BiConsumer<String, String> onTransfer, Consumer<RuntimeException> onErr) {
        Map<String, String> xfs = transfers.get(state);
        if (xfs == null) {
            onErr.accept(new UndefinedStateException(state, "state(" + state + ") not defined"));
        } else {
            String newState = xfs.get(evt);
            if (newState == null) {
                onErr.accept(new EventCantRespondException(state, evt, "state(" + state + ") cannot respond for event " + evt));
            } else {
                String oldState = this.state;
                this.state = newState;
                onTransfer.accept(oldState, newState);
            }
        }
        return this;
    }

    public StatefulSTM transfer(String evt, BiConsumer<String, String> onTransfer) {
        return transfer(evt, onTransfer, e -> {
            throw e;
        });
    }


    public boolean canRespond(String evt) {
        return transfers.get(state) == null || transfers.get(state).containsKey(evt);
    }

    public Set<String> respondEvents() {
        Map<String, String> xfs = transfers.get(state);
        if (xfs == null) {
            return treeSet();
        } else {
            return xfs.keySet();
        }
    }
}