package fn.st;

public class UndefinedStateException extends RuntimeException {
    private String state;

    public String getState() {
        return state;
    }

    public UndefinedStateException(String state, String msg) {
        super(msg);
        this.state = state;
    }
}