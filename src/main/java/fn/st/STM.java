package fn.st;

import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static fn.Colls.treeSet;

/**
 * 用于无状态的状态机。状态机只记录状态转换关系，状态由调用者自己记录。
 */
public class STM {
    private STM(){}
    private Map<String, Map<String, String>> transfers;

    public static STM defStm(Map<String, Map<String, String>> transfers) {
        STM stm = new STM();
        stm.transfers = transfers;
        return stm;
    }

    public STM transfer(String state, String evt, BiConsumer<String, String> onTransfer, Consumer<RuntimeException> onErr) {
        Map<String, String> xfs = transfers.get(state);
        if (xfs == null) {
            onErr.accept(new UndefinedStateException(state, "state(" + state + ") not defined"));
        } else {
            String newState = xfs.get(evt);
            if (newState == null) {
                onErr.accept(new EventCantRespondException(state, evt, "state(" + state + ") cannot respond for event " + evt));
            } else {
                onTransfer.accept(state, newState);
            }
        }
        return this;
    }

    public STM transfer(String state, String evt, BiConsumer<String, String> onTransfer) {
        return transfer(state, evt, onTransfer, e -> {
            throw e;
        });
    }

    public boolean canRespond(String state, String evt) {
        return transfers.get(state) == null || transfers.get(state).containsKey(evt);
    }

    public Set<String> respondEvents(String state) {
        Map<String, String> xfs = transfers.get(state);
        if (xfs == null) {
            return treeSet();
        } else {
            return xfs.keySet();
        }
    }

}