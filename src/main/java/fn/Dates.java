package fn;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public abstract class Dates {

    protected static final Map<String, DateTimeFormatter> formatters = new ConcurrentHashMap<>();

    //ignore-mixin-begin
    private Dates() {
    }

    //ignore-mixin-end
    private static DateTimeFormatter ofPattern(String pattern) {
        return formatters.computeIfAbsent(pattern, p -> DateTimeFormatter.ofPattern(p));
    }
    //format

    public static String format(LocalDate date) {
        return DateTimeFormatter.ISO_LOCAL_DATE.format(date);
    }


    public static String format(LocalDate date, String pattern) {
        return date.format(ofPattern(pattern));
    }


    public static String format(LocalDateTime date) {
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(date);
    }


    public static String format(LocalDateTime date, String pattern) {
        return date.format(ofPattern(pattern));
    }


    public static String format(LocalTime date, String pattern) {
        return date.format(ofPattern(pattern));
    }


    public static String format(LocalTime date) {
        return DateTimeFormatter.ISO_LOCAL_TIME.format(date);
    }


    public static String format(Date date) {
        return format(LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
    }


    public static String format(Date date, String pattern) {
        return date.toInstant().atZone(ZoneId.systemDefault()).format(ofPattern(pattern));
    }


    //toLocalDate
    public static LocalDate toLocalDate(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
    }


    public static LocalDate toLocalDate(String date, String pattern) {
        return LocalDate.parse(date, ofPattern(pattern));
    }


    public static LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    //toLocalDateTime

    public static LocalDateTime toLocalDateTime(String date) {
        return LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }


    public static LocalDateTime toLocalDateTime(String date, String pattern) {
        return LocalDateTime.parse(date, ofPattern(pattern));
    }


    public static LocalDateTime toLocalDateTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    //toDate

    public static Date toDate(String date) {
        return toDate(toLocalDateTime(date));
    }


    public static Date toDate(String date, String pattern) {
        return toDate(toLocalDateTime(date, pattern));
    }


    public static Date toDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }


    public static Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
