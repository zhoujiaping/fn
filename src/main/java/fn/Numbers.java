package fn;

public class Numbers {
    public static boolean isDivisible(long x, long y) {
        while (y % 2 == 0) {
            y = y / 2;
        }
        while (y % 5 == 0) {
            y = y / 2;
        }
        return x % y == 0;
    }

    public static void main(String[] args) {
        System.out.println(isDivisible(1,3));
        System.out.println(isDivisible(1,30));
        System.out.println(isDivisible(2,30));
    }
}
