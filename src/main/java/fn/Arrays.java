package fn;

import java.lang.reflect.Array;
import java.util.function.*;

public abstract class Arrays {
    //ignore-mixin-begin
    private Arrays() {
    }
    //ignore-mixin-end

    public static boolean isArray(Object o) {
        return o == null || o.getClass().isArray();
    }
    public static boolean isStrictArray(Object o) {
        return o != null && o.getClass().isArray();
    }

    public static <T> T[] array(T... items) {
        return items;
    }


    public static <T> T[] newArray(Class<T> clazz, int size) {
        return (T[]) Array.newInstance(clazz, size);
    }

    public static <T> void each(T[] arr, Consumer<T> fn) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            fn.accept(arr[i]);
        }
    }

    public static <T> void each(T[] arr, Predicate<T> fn) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            if (!Boolean.TRUE.equals(fn.test(arr[i]))) {
                return;
            }
        }
    }

    /**
     * travel array elements with index
     */
    public static <T> void each(T[] arr, BiConsumer<Integer, T> fn) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            fn.accept(i, arr[i]);
        }
    }

    public static <T> void each(T[] arr, BiPredicate<Integer, T> fn) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            if (!Boolean.TRUE.equals(fn.test(i, arr[i]))) {
                return;
            }
        }
    }

    public static <T> T getAt(T[] arr, int idx) {
        if(idx<0){
            idx+=arr.length;
        }
        return arr[idx];
    }
}
