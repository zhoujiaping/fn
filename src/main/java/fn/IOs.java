package fn;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.springframework.util.FileCopyUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.BiConsumer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public abstract class IOs {
    //ignore-mixin-begin
    private IOs(){}
    //ignore-mixin-end
    public static <T> T println( Object obj) {
        System.out.println(Strs.format("%s", obj));
        return null;
    }

    /**
     * the function System.out.printf is too strict.
     * if number of placeholder more than number of args,
     * it will throw a exception.
     * this is a relaxed version of System.out.printf
     */

    public static void printf( String template,  Object... args) {
        System.out.println(Strs.format(template, args));
    }
    @SneakyThrows
    public static void zip(InputStream input, OutputStream output, String entryName) {
        @Cleanup ZipOutputStream zos = new ZipOutputStream(output);
        zos.putNextEntry(new ZipEntry(entryName));
        FileCopyUtils.copy(input, zos);
        zos.closeEntry();
    }

    @SneakyThrows
    public static void unZip(InputStream input, BiConsumer<String, ZipInputStream> consumer) {
        @Cleanup ZipInputStream zis = new ZipInputStream(input);
        ZipEntry entry = zis.getNextEntry();
        consumer.accept(entry.getName(), zis);
        zis.closeEntry();
    }

}

