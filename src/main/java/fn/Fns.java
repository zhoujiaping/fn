package fn;

import java.util.function.*;

public abstract class Fns {
    //ignore-mixin-begin
    private Fns(){}
    //ignore-mixin-end

    public static <T> Consumer<T> fn(Consumer<T> fn) {
        return fn;
    }
    public static <T,U> BiConsumer<T,U> fn(BiConsumer<T,U> fn) {
        return fn;
    }
    public static <T> Supplier<T> fn(Supplier<T> fn) {
        return fn;
    }
    public static <T,R> Function<T,R> fn(Function<T,R> fn) {
        return fn;
    }
    public static <T,U,R> BiFunction<T,U,R> fn(BiFunction<T,U,R> fn) {
        return fn;
    }
    public static <T> T get(Supplier<T> f){
        return f.get();
    }

}
