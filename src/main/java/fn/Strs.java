package fn;

import com.google.common.base.Ascii;
import com.google.common.base.Strings;
import com.google.common.hash.Hashing;
import fn.utils.GenericTokenParser;
import lombok.val;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public abstract class Strs {
    //ignore-mixin-begin
    private Strs(){}
    //ignore-mixin-end
    protected static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

    public static String str(Object... items) {
        if (items == null) {
            return "";
        }
        val sb = new StringBuilder();
        for (int i = 0; i < items.length; i++) {
            sb.append(items[i]);
        }
        return sb.toString();
    }

    public static String padStart(String str, int len, char padding) {
        return Strings.padStart(str, len, padding);
    }


    public static String padEnd(String str, int len, char padding) {
        return Strings.padStart(str, len, padding);
    }


    public static String repeat(String str, int times) {
        return Strings.repeat(str, times);
    }


    public static String sha256(CharSequence input) {
        return Hashing.sha256().hashString(input, StandardCharsets.UTF_8).toString();
    }


    public static String sha256(byte[] input) {
        return Hashing.sha256().hashBytes(input).toString();
    }


    public static String sha512(CharSequence input) {
        return Hashing.sha512().hashString(input, StandardCharsets.UTF_8).toString();
    }


    public static String sha512(byte[] input) {
        return Hashing.sha512().hashBytes(input).toString();
    }


    public static String format(String str, Object... args) {
        return Strings.lenientFormat(str, args);
    }

    public static String fmtMap(String str, String openToken, String closeToken, Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            return str;
        }
        return new GenericTokenParser(openToken, closeToken, token -> {
            Object value = map.get(token);
            if (value == null && !map.containsKey(token)) {
                return openToken + token + closeToken;
            }
            return String.valueOf(value);
        }).parse(str);
    }

    public static String fmt(String str, String openToken, String closeToken, Object... args) {
        if (args == null || args.length == 0) {
            return str;
        }
        int[] idx = new int[]{0};
        return new GenericTokenParser(openToken, closeToken, token -> {
            if (token.equals("")) {
                if (idx[0] >= args.length) {
                    return openToken + closeToken;
                }
                return String.valueOf(args[idx[0]++]);
            }
            int i = Integer.parseInt(token);
            if (i >= args.length) {
                return openToken + i + closeToken;
            }
            return String.valueOf(args[i]);
        }).parse(str);
    }

    public static String truncate(CharSequence seq, int maxLength, String truncationIndicator) {
        return Ascii.truncate(seq, maxLength, truncationIndicator);
    }


    public static String hex(byte[] buf) {
        char[] charArray = new char[buf.length * 2];
        int j = 0;
        for (int i = 0; i < buf.length; i++) {
            charArray[j++] = HEX_CHARS[buf[i] >>> 4 & 0x0F];
            //high 4 bit, then low 4 bit
            charArray[j++] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(charArray);
    }
}
