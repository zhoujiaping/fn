package fn.tco;

import java.util.stream.Stream;

public class Tco {
    /**
     用一个函数式接口模拟栈帧
     */
    public interface TailRecursion<T> {
        /**
         * 用于递归栈帧之间的连接,惰性求值
         *
         * @return 下一个递归栈帧
         */
        TailRecursion<T> apply();

        /**
         * 判断当前递归是否结束
         *
         * @return 默认为false, 因为正常的递归过程中都还未结束
         */
        default boolean isFinished() {
            return false;
        }

        /**
         * 获得递归结果,只有在递归结束才能调用,这里默认给出异常,通过工具类的重写来获得值
         *
         * @return 递归最终结果
         */
        default T getResult() {
            throw new RuntimeException("tail recursion not finished!");
        }

        /**
         * 及早求值,执行者一系列的递归,因为栈帧只有一个,所以使用findFirst获得最终的栈帧,接着调用getResult方法获得最终递归值
         *
         * @return 及早求值, 获得最终递归结果
         */
        default T invoke() {
            TailRecursion<T> recur = this;
            while(!recur.isFinished()){
                recur = recur.apply();
            }
            return recur.getResult();
//            return Stream.iterate(this, TailRecursion::apply)
//                    .filter(TailRecursion::isFinished)
//                    .findFirst()
//                    .get()
//                    .getResult();
        }
        /**
         * 统一结构的方法,获得当前递归的下一个递归
         *
         * @param nextFrame 下一个递归
         * @param <T>       T
         * @return 下一个递归
         */
        static <T> TailRecursion<T> recur(TailRecursion<T> nextFrame) {
            return nextFrame;
        }

        /**
         * 结束当前递归，重写对应的默认方法的值,完成状态改为true,设置最终返回结果,设置非法递归调用
         *
         * @param value 最终递归值
         * @param <T>   T
         * @return 一个isFinished状态true的尾递归, 外部通过调用接口的invoke方法及早求值, 启动递归求值。
         */
        static <T> TailRecursion<T> done(T value) {
            return new TailRecursion<T>() {
                @Override
                public TailRecursion<T> apply() {
                    throw new RuntimeException("tail recursion was done!");
                }

                @Override
                public boolean isFinished() {
                    return true;
                }

                @Override
                public T getResult() {
                    return value;
                }
            };
        }
    }


    public static void main(String[] args) {
        System.out.println(add(1,100000).invoke());
    }

    public static TailRecursion<Integer> add(int x, int y) {
        if (y > 0) {
            return TailRecursion.recur(()->add(x + 1, y - 1));
        } else {
            return TailRecursion.done(x);
        }
    }

}
