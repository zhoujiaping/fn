package fn.tco;

import java.util.function.Supplier;

/**
 * 蹦床函数实现尾递归优化
 * 参考实现
 * function trampoline(f) {
 * while (f && f instanceof Function) {
 * f = f()
 * }
 * return f
 * }
 * <p>
 * function f(n, a = 0, b = 1) {
 * if (n > 0) {
 * [a, b] = [b, a + b]
 * return f.bind(null, n - 1, a, b)
 * } else {
 * return a
 * }
 * }
 * <p>
 * <p>
 * trampoline(100000); //正常运行
 */
public class Tco2 {
    public static <T> Supplier<T> recur(Supplier<T> fn) {
        return fn;
    }

    public static <T> T trampoline(Object fn) {
        while (fn instanceof Supplier) {
            fn = ((Supplier<?>) fn).get();
        }
        return (T) fn;
    }

    private static Object add(int x, int y) {
        if (y > 0) {
            return recur(() -> add(x + 1, y - 1));
        } else {
            return x;
        }
    }

    public static void main(String[] args) {
        int ans = trampoline(add(1, 1000000));
        System.out.println(ans);
    }

}
