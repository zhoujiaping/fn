package fn.tco;

import lombok.SneakyThrows;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.implementation.bind.annotation.*;
import org.apache.commons.lang3.mutable.MutableObject;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.*;

/**
 * 参考
 * function tco(f) {
 *   var value;
 *   var active = false;
 *   var accumulated = [];
 *   return function accumulator() {
 *     accumulated.push(arguments);
 *     if (!active) {
 *       active = true;
 *       while (accumulated.length) {
 *         value = f.apply(this, accumulated.shift());
 *       }
 *       active = false;
 *       return value;
 *     }
 *   };
 * }
 * const sum = tco(function (x,y){
 *   if (y > 0) {
 *     return sum(x + 1, y - 1)
 *   }
 *   else {
 *     return x
 *   }
 * })
 * console.info(sum(1,10000))
 */
public class Tco3 {
    static ThreadLocal<Map<Method,State>> stateHolder = ThreadLocal.withInitial(HashMap::new);

    static class State {
        Object value;
        boolean active;
        LinkedList<Object[]> accumulated = new LinkedList<>();
    }
    @SneakyThrows
    @RuntimeType
    public static Object intercept(@AllArguments Object[] args,
                                   @This(optional = true) Object target,
                                   @Origin Method method,
                                   @Origin Class<?> origClass,
                                   @SuperMethod(nullIfImpossible = true) Method superMethod) {
        Map<Method,State> stateMap = stateHolder.get();
        State st = stateMap.get(method);
        if(st==null){
            try{
                stateMap.put(method,new State());
                if(!method.isAccessible()){
                    method.setAccessible(true);
                }
                return method.invoke(target,args);
            }finally {
                stateMap.remove(method);
            }
        }else{
            st.accumulated.add(args);
            if (!st.active) {
                st.active = true;
                while (!st.accumulated.isEmpty()) {
                    st.value = superMethod.invoke(target,st.accumulated.pop());
                }
                st.active = false;
                return st.value;
            }
            return fn.Objects.defaultValue(method.getReturnType());
        }
    }
    @SneakyThrows
    public static <T> Class<? extends T> tco(Class<T> targetClass) {
        Objects.requireNonNull(targetClass, "targetClass can't be null");
        if (ClassUtils.isPrimitiveOrWrapper(targetClass)) {
            throw new RuntimeException("can't proxy a primitive or wrapper class");
        }
        DynamicType.Builder<T> builder = new ByteBuddy().subclass(targetClass);
        MutableObject<DynamicType.Builder<T>> builderHolder = new MutableObject<>(builder);
        List<Method> proxyMethods = new LinkedList<>();
        ReflectionUtils.doWithMethods(targetClass, method -> {
                    builderHolder.setValue(
                            builderHolder.getValue()
                                    .define(method).intercept(MethodDelegation.to(Tco3.class))
                    );
                    proxyMethods.add(method);
                },
                method -> method.getAnnotation(TailRecur.class)!=null);
        Class<? extends T> generated = builderHolder.getValue()
                .make()
                .load(Thread.currentThread().getContextClassLoader())
                .getLoaded();
        return generated;
    }
}
