package fn;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.SneakyThrows;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.SerializationUtils;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Ref;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

public abstract class Objects {
    //ignore-mixin-begin
    private Objects() {
    }

    //ignore-mixin-end
    protected static final Map<Class<?>, Object> defaultValues = new HashMap<>();

    protected static final Cache<Class<?>, Function<Object, String>> toStrCache =
            CacheBuilder.newBuilder().expireAfterAccess(10, TimeUnit.MINUTES).build();

    public static final DefaultConversionService converterService = new DefaultConversionService();

    static {
        defaultValues.put(boolean.class, Boolean.valueOf(false));
        defaultValues.put(byte.class, 0);
        defaultValues.put(short.class, Short.valueOf((short) 0));
        defaultValues.put(int.class, 0);
        defaultValues.put(long.class, 0L);
        defaultValues.put(float.class, 0f);
        defaultValues.put(double.class, 0d);
        defaultValues.put(char.class, '\u0000');
    }

    @SneakyThrows
    public static String toStr(Object o) {
        if (o == null) {
            return "null";
        } else if (ClassUtils.isPrimitiveOrWrapper(o.getClass())) {
            return o.toString();
        }
        Method toStringMethod = ReflectionUtils.findMethod(o.getClass(), "toString");
        if (toStringMethod.getDeclaringClass() != Object.class) {
            return o.toString();
        }
        List<String> nameValues = new ArrayList<>();
        ReflectionUtils.doWithFields(o.getClass(), f -> {
            ReflectionUtils.makeAccessible(f);
            nameValues.add(f.getName() + "=" + ReflectionUtils.getField(f, o));
        }, f -> !Modifier.isStatic(f.getModifiers()));
        return o.getClass().getSimpleName() + "@" + System.identityHashCode(o) + "(" + nameValues.stream().collect(Collectors.joining(",")) + ")";
    }

    public static <T> T cast(Object o) {
        return (T) o;
    }

    public static <T> T cast(Object o, Class<T> clazz) {
        return (T) o;
    }

    public static <T> T orElse(T t, T elseValue) {
        return t != null ? t : elseValue;
    }

    public static <T> T orElseGet(T t, Supplier<T> supplier) {
        if (t != null) {
            return t;
        }
        return supplier.get();
    }

    public static boolean isInstanceOf(Object o, Class<?> clazz) {
        return o != null && clazz.isAssignableFrom(o.getClass());
    }

    public static <T> T deepCopy(T obj) {
        byte[] bytes = SerializationUtils.serialize(obj);
        return (T) SerializationUtils.deserialize(bytes);
    }

    public static Object defaultValue(Class<?> clazz) {
        return defaultValues.get(clazz);
    }

    public static boolean asBoolean(Object o) {
        if (o == null) {
            return false;
        }
        //boolean
        if (o instanceof Boolean) {
            return (boolean) o;
        }
        //collections and array
        if (o.getClass().isArray()) {
            return Array.getLength(o) > 0;
        }
        if (o instanceof Collection) {
            return !((Collection) o).isEmpty();
        }
        //Matcher
        if (o instanceof Matcher) {
            return ((Matcher) o).find(0);
        }
        //Iterators and Enumerations
        if (o instanceof Iterator) {
            return ((Iterator) o).hasNext();
        }
        if (o instanceof Enumeration) {
            return ((Enumeration) o).hasMoreElements();
        }
        //Maps
        if (o instanceof Map) {
            return !((Map) o).isEmpty();
        }
        //strings
        if (o instanceof CharSequence) {
            return ((CharSequence) o).length() > 0;
        }
        if (o instanceof Character) {
            return (char) o != 0;
        }
        //numbers
        if (o instanceof Number) {
            return ((Number) o).doubleValue() != 0.0D;
        }
        return true;
    }

    public static <T> T convert(Object obj, Class<T> clazz) {
        return converterService.convert(obj, clazz);
    }

    /**
     * Object#equals
     */
    public static boolean strictEq(Object o1, Object o2) {
        if (o1 == null) {
            throw new IllegalArgumentException("o1 is null");
        } else if (o2 == null) {
            throw new IllegalArgumentException("o2 is null");
        } else if (!o1.getClass().equals(o2.getClass())) {
            throw new IllegalArgumentException("type not match");
        } else {
            return o1.equals(o2);
        }
    }
}
