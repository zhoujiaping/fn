package fn.utils;

import com.google.common.base.Throwables;

import java.util.Optional;

public abstract class Exceptions {
    public static String location(Throwable e) {
        StackTraceElement[] traces = e.getStackTrace();
        StackTraceElement trace = traces[0];
        String className = trace.getClassName();
        String methodName = trace.getMethodName();
        String fileName = trace.getFileName();
        int lineNumber = trace.getLineNumber();
        return "at " + className + "." + methodName + "(" + fileName + ":" + lineNumber + ")";
    }
    public static Optional<StackTraceElement> trace(Throwable t){
        Throwable rootCause = Throwables.getRootCause(t);
        StackTraceElement[] traces = rootCause.getStackTrace();
        if(traces.length == 0){
            return Optional.empty();
        }
        return Optional.of(traces[0]);
    }
}
