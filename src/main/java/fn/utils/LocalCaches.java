package fn.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public abstract class LocalCaches {
    private static final ThreadLocal<Map<String,Map<String,Object>>> holder= new ThreadLocal<>();
    public static <T> T withCache(Supplier<T> fn){
        try{
            Map<String, Map<String,Object>> caches = holder.get();
            if(caches != null){
                throw new RuntimeException("cache was enabled already!");
            }
            holder.set(new HashMap<>());
            return fn.get();
        }finally{
            holder.remove();
        }
    }

    public static <T> Map<String,T> getCache(String cacheName, Supplier<Map<String,T>> cacheSupplier){
        Map<String,Map<String,Object>> caches = holder.get();
        if(caches == null){
            return null;
        }
        Map<String,T> cache = (Map<String,T>)caches.get(cacheName);
        if(cache == null){
            cache = cacheSupplier.get();
            if(cache == null){
                throw new RuntimeException("cache cannot be null");
            }
            caches.put(cacheName,(Map<String,Object>)cache);
        }
        return cache;
    }

    public static <T> T getNonnull(String cacheName, String cacheKey, Supplier<T> valueSupplier){
        Map<String,T> cache= getCache(cacheName,()->new HashMap<>());
        if(cache == null){
            return valueSupplier.get();
        }
        Object v = cache.get(cacheKey);
        if(v == null){
            v= valueSupplier.get();
            if(v == null){
                throw new RuntimeException("value cannot be null!");
            }
            cache.put(cacheKey,(T)v);
        }
        return (T)v;
    }
    public static <T> T get(String cacheName,String cacheKey,Supplier<Map<String,T>> cacheSupplier){
        Map<String,T> cache= getCache(cacheName,cacheSupplier);
        if(cache == null){
            throw new RuntimeException("caches not set");
        }
        return cache.get(cacheKey);
    }
    public static String key(Object... rest){
        return Arrays.stream(rest).map(String::valueOf).collect(Collectors.joining("#"));
    }
}
