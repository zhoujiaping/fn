package fn.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

public abstract class Enums {

    public static <E extends Enum<E>> Map<String, E> enumMap(Class<E> enumClass) {
        final Map<String, E> map = new LinkedHashMap<>();
        for (final E e : enumClass.getEnumConstants()) {
            map.put(e.name(), e);
        }
        return map;
    }

    public static <K, V, E extends Enum<E>> Map<K, V> enumMap(Class<E> enumClass, Function<E, K> kf, Function<E, V> vf) {
        final Map<K, V> map = new LinkedHashMap<>();
        for (final E e : enumClass.getEnumConstants()) {
            map.put(kf.apply(e), vf.apply(e));
        }
        return map;
    }
}
