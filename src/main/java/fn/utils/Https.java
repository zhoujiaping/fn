package fn.utils;

import com.sun.net.httpserver.HttpServer;
import lombok.SneakyThrows;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

public abstract class Https {
    //get
    //post
    //delete
    //put
    //upload
    //download
    //export
    @SneakyThrows
    public static void startServer() {
        HttpServer server = HttpServer.create(new InetSocketAddress(8001), 0);
        server.createContext("/test", exchange -> {
            try {
                exchange.sendResponseHeaders(200,"hello test".getBytes(StandardCharsets.UTF_8).length);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(exchange.getResponseBody()));
                bw.write("hello test");
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        server.createContext("/world", exchange -> {
            try {
                exchange.sendResponseHeaders(200,"hello world".getBytes(StandardCharsets.UTF_8).length);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(exchange.getResponseBody()));
                bw.write("hello world");
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        server.createContext("/chunk", exchange -> {
            try {
                //exchange.getResponseHeaders().add("Transfer-Encoding","chunked");
                exchange.getResponseHeaders().add("Content-Type","text/plain;charset=UTF-8");
                //Content-Length传0自动被走Transfer-Encoding: chunked
                exchange.sendResponseHeaders(200,0);
                BufferedOutputStream out = new BufferedOutputStream(exchange.getResponseBody());
                String[] strs = new String[]{"hello","world","jack","jackson"};
                for (String str : strs) {
                    out.write(str.getBytes(StandardCharsets.UTF_8));
                }
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        server.start();
    }

    public static void main(String[] args) {
        startServer();
    }

}
