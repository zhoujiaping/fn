package fn.utils;

import com.google.common.collect.Lists;
import fn.Colls;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class Streams {

    public static <T> Stream<List<T>> partition(Stream<T> stream, int n) {
        Iterator<T> iter = stream.iterator();
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<List<T>>() {
            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public List<T> next() {
                List<T> part = new ArrayList<>();
                while (iter.hasNext() && part.size() < n) {
                    part.add(iter.next());
                }
                return part;
            }
        }, Spliterator.ORDERED), false);
    }

    public static <T> Stream<T> take(Stream<T> stream, int n) {
        return stream.limit(n);
    }

    public static <T> Stream<T> takeWhile(Stream<T> stream, Predicate<T> pred) {
        Iterator<T> iter = stream.iterator();
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<T>() {
            T n;

            @Override
            public boolean hasNext() {
                if (!iter.hasNext()) {
                    return false;
                }
                n = iter.next();
                return pred.test(n);
            }

            @Override
            public T next() {
                T ret = n;
                n = null;
                return ret;
            }
        }, Spliterator.ORDERED), false);
    }

    public static <T> Stream<T> drop(Stream<T> stream, int n) {
        return stream.skip(n);
    }

    public static <T> Stream<T> dropWhile(Stream<T> stream, Predicate<T> pred) {
        Iterator<T> iter = stream.iterator();
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<T>() {
            T first;
            boolean dropped = false;
            boolean isFirst = true;

            @Override
            public boolean hasNext() {
                if (dropped) {
                    return iter.hasNext();
                }
                dropped = true;
                while (iter.hasNext()) {
                    T x = iter.next();
                    if (!pred.test(x)) {
                        first = x;
                        return true;
                    }
                }
                return false;
            }

            @Override
            public T next() {
                if (isFirst) {
                    isFirst = false;
                    T ret = first;
                    first = null;
                    return ret;
                }
                return iter.next();
            }
        }, Spliterator.ORDERED), false);
    }

    public static void main(String[] args) {
        System.out.println(drop(Lists.newArrayList("a", "b", "c", "d", "e").stream(), 5).collect(Collectors.toList()));
        System.out.println(take(Lists.newArrayList("a", "b", "c", "d", "e").stream(), 2).collect(Collectors.toList()));
        System.out.println(take(Lists.newArrayList("a", "b", "c", "d", "e").stream(), 20).collect(Collectors.toList()));
        System.out.println(takeWhile(Lists.newArrayList("a", "b", "c", "d", "a", "e").stream(), it -> it.compareTo("c") <= 0).collect(Collectors.toList()));
        System.out.println(dropWhile(Lists.newArrayList("a", "b", "c", "d", "a", "e").stream(), it -> it.compareTo("c") <= 0).collect(Collectors.toList()));
        partition(Colls.range(100,101).boxed(),10).forEach(it->{
            System.out.println(it);
        });
    }
}
