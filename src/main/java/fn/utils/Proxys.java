package fn.utils;

import fn.internal.BuddyInterceptor;
import fn.internal.ProxyHandler;

import java.lang.reflect.Method;
import java.util.function.Predicate;

public abstract class Proxys {

    public static <T> Class<? extends T> proxy(Class<T> targetClass,
                                               ProxyHandler handler, Predicate<Method> filter) {
        return BuddyInterceptor.proxy(targetClass, handler,filter);
    }

    public static <T> Class<? extends T> proxy(Class<T> targetClass,
                                               ProxyHandler handler) {
        return BuddyInterceptor.proxy(targetClass, handler);
    }
}
