package fn.utils;

import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public abstract class Bindings {
    public static <T,R> R withBinding(ThreadLocal<T> tl, UnaryOperator<T> rebind, Supplier<R> fn){
        T oldVal = tl.get();
        R ret;
        try{
            tl.set(rebind.apply(oldVal));
            ret = fn.get();
        }finally{
            if(oldVal==null){
                tl.remove();
            }else{
                tl.set(oldVal);
            }
        }
        return ret;
    }
}
