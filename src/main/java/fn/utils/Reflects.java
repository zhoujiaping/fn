package fn.utils;

import fn.internal.Reflections;
import org.springframework.objenesis.ObjenesisHelper;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public abstract class Reflects {
    public static <T> T forceNew(Class<T> clazz) {
        return ObjenesisHelper.newInstance(clazz);
    }
    //reflect
    
    public static void eachFields( Class<?> clazz,  Consumer<Field> consumer,  Predicate<Field> fieldPredicate) {
        ReflectionUtils.doWithFields(clazz, f -> consumer.accept(f), f -> fieldPredicate.test(f));
    }

    /***
     * spring ReflectionUtils#findMethod need exactly parameter types.
     * we need a method like metaClass#respondsTo in groovy
     */
    
    public static  List<Method> responseTo( Class<?> clazz,  String method, Object... args) {
        return Reflections.responseTo(clazz, method, args);
    }

    
    public static  <T> T invokeMethod( Method method, Object target, Object... args) {
        ReflectionUtils.makeAccessible(method);
        return (T) ReflectionUtils.invokeMethod(method, target, args);
    }

}
