package fn.utils;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public abstract class Resources {
    public static final DefaultResourceLoader loader = new DefaultResourceLoader();
    @SneakyThrows
    public static String readText(String location){
        Resource r = loader.getResource(location);
        @Cleanup InputStream in = r.getInputStream();
        return StreamUtils.copyToString(in, StandardCharsets.UTF_8);
    }
    @SneakyThrows
    public static byte[] readBytes(String location){
        Resource r = loader.getResource(location);
        @Cleanup InputStream in = r.getInputStream();
        return StreamUtils.copyToByteArray(in);
    }
}
