package fn;

import com.google.common.collect.Sets;
import fn.tuple.Tuple2;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.objenesis.ObjenesisHelper;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public abstract class Colls {
    //ignore-mixin-begin
    private Colls() {
    }

    //ignore-mixin-end
    //coll fn
    public static <T> LinkedList<T> linkedList(T... args) {
        LinkedList<T> coll = new LinkedList<>();
        if (args == null) {
            return coll;
        }
        Collections.addAll(coll, args);
        return coll;
    }

    public static <T> ArrayList<T> arrayList(T... args) {
        ArrayList<T> coll = new ArrayList<>();
        if (args == null) {
            return coll;
        }
        Collections.addAll(coll, args);
        return coll;
    }

    public static <V> HashSet<V> hashSet(V... vals) {
        HashSet<V> coll = new HashSet<>();
        if (vals == null) {
            return coll;
        }
        Collections.addAll(coll, vals);
        return coll;
    }

    public static <V> TreeSet<V> treeSet(V... vals) {
        TreeSet<V> coll = new TreeSet<>();
        if (vals == null) {
            return coll;
        }
        Collections.addAll(coll, vals);
        return coll;
    }

    public static <K, V> HashMap<K, V> hashMap(Object... kvs) {
        HashMap<K, V> map = new HashMap<>();
        if (kvs == null) {
            return map;
        }
        if (kvs.length % 2 == 1) {
            throw new IllegalArgumentException("kvs length must be even!");
        }
        for (int i = 0; i < kvs.length; i = i + 2) {
            map.put((K) kvs[i], (V) kvs[i + 1]);
        }
        return map;
    }

    public static <K, V> LinkedHashMap<K, V> linkedHashMap(Object... kvs) {
        LinkedHashMap<K, V> map = new LinkedHashMap<>();
        if (kvs == null) {
            return map;
        }
        if (kvs.length % 2 == 1) {
            throw new IllegalArgumentException("kvs length must be even!");
        }
        for (int i = 0; i < kvs.length; i = i + 2) {
            map.put((K) kvs[i], (V) kvs[i + 1]);
        }
        return map;
    }

    public static <K, V> SortedMap<K, V> treeMap(Object... kvs) {
        TreeMap<K, V> map = new TreeMap<>();
        if (kvs == null) {
            return map;
        }
        if (kvs.length % 2 == 1) {
            throw new IllegalArgumentException("kvs length must be even!");
        }
        for (int i = 0; i < kvs.length; i = i + 2) {
            map.put((K) kvs[i], (V) kvs[i + 1]);
        }
        return map;
    }

    public static <E> List<E> empty(List<E> coll) {
        if (coll instanceof LinkedList) {
            return new LinkedList<>();
        }
        return new ArrayList<>();
    }

    public static <E> Set<E> empty(Set<E> coll) {
        if (coll instanceof TreeSet) {
            return new TreeSet<>();
        }
        return new HashSet<>();
    }

    public static <K, V> Map<K, V> empty(Map<K, V> coll) {
        if (coll instanceof TreeMap) {
            return new TreeMap<>();
        }else if(coll instanceof LinkedHashMap){
            return new LinkedHashMap<>();
        }
        return new HashMap<>();
    }

    public static <V1, V2> List<V2> map(List<V1> coll, Function<V1, V2> fn) {
        List<V2> newColl;
        if (coll instanceof LinkedList) {
            newColl = new LinkedList<>();
        } else {
            newColl = new ArrayList<>();
        }
        for (V1 v1 : coll) {
            newColl.add(fn.apply(v1));
        }
        return newColl;
    }

    public static <V1, V2> Set<V2> map(Set<V1> coll, Function<V1, V2> fn) {
        Set<V2> newColl = (Set<V2>) empty(coll);
        for (V1 v1 : coll) {
            newColl.add(fn.apply(v1));
        }
        return newColl;
    }

    public static <V1, V2> List<V2> map(List<V1> coll, BiFunction<Integer, V1, V2> fn) {
        List<V2> newColl = (List<V2>) empty(coll);
        int idx = 0;
        for (V1 v1 : coll) {
            newColl.add(fn.apply(idx++, v1));
        }
        return newColl;
    }

    public static <K1,K2, V1, V2> Map<K2, V2> map(Map<K1, V1> coll, BiFunction<K1, V1, K2> kf,BiFunction<K1,V1,V2> vf) {
        Map<K2, V2> newColl = (Map<K2, V2>) empty(coll);
        coll.forEach((k, v) ->
                newColl.put(kf.apply(k,v), vf.apply(k, v))
        );
        return newColl;
    }

    public static <T> T reduce(Collection<T> coll, BiFunction<T, T, T> fn) {
        Iterator<T> iter = coll.iterator();
        if (!iter.hasNext()) {
            throw new IllegalArgumentException("coll can't be empty!");
        }
        T res = iter.next();
        while (iter.hasNext()) {
            res = fn.apply(res, iter.next());
        }
        return res;
    }

    public static <T, V> T reduce(Collection<V> coll, BiFunction<T, V, T> fn, T initVal) {
        Iterator<V> iter = coll.iterator();
        T res = initVal;
        while (iter.hasNext()) {
            res = fn.apply(res, iter.next());
        }
        return res;
    }

    public static <C extends List<T>, T> C filter(C coll, Function<T, Boolean> fn) {
        List<T> newColl = empty(coll);
        for (T item : coll) {
            if (Boolean.TRUE.equals(fn.apply(item))) {
                newColl.add(item);
            }
        }
        return (C) newColl;
    }

    public static <C extends List<T>, T> C filter(C coll, BiFunction<Integer, T, Boolean> fn) {
        List<T> newColl = empty(coll);
        int idx = 0;
        for (T item : coll) {
            if (Boolean.TRUE.equals(fn.apply(idx++, item))) {
                newColl.add(item);
            }
        }
        return (C) newColl;
    }

    public static <C extends Set<T>, T> C filter(C coll, Function<T, Boolean> fn) {
        Set<T> newColl = empty(coll);
        for (T item : coll) {
            if (Boolean.TRUE.equals(fn.apply(item))) {
                newColl.add(item);
            }
        }
        return (C) newColl;
    }

    public static <C extends Map<K, V>, K, V> C filter(C coll, BiPredicate<K, V> fn) {
        Map<K, V> newColl = empty(coll);
        for (Map.Entry<K, V> item : coll.entrySet()) {
            if (Boolean.TRUE.equals(fn.test(item.getKey(), item.getValue()))) {
                newColl.put(item.getKey(), item.getValue());
            }
        }
        return (C) newColl;
    }

    public static <K1,V1,K2,V2> Map<K2,V2> zipMap(Iterable<Map.Entry<K1,V1>> entries, Function<K1,K2> kf,Function<V1,V2> vf){
        Map<K2,V2> map = new LinkedHashMap<>();
        entries.forEach(it->map.put(kf.apply(it.getKey()),vf.apply(it.getValue())));
        return map;
    }

//    public static <K1,V1,K2,V2> Map<K2,V2> zipMap(Iterable<Tuple2<K1,V1>> entries, Function<K1,K2> kf,Function<V1,V2> vf){
//        Map<K2,V2> map = new LinkedHashMap<>();
//        entries.forEach(it->map.put(kf.apply(it._1),vf.apply(it._2)));
//        return map;
//    }

    public static <K, V> Map<K, V> zipMap(Iterable<K> keys, Iterable<V> values) {
        Map<K, V> map = new HashMap<>();
        Iterator<K> keysIter = keys.iterator();
        Iterator<V> valuesIter = values.iterator();
        while (keysIter.hasNext()) {
            if (valuesIter.hasNext()) {
                map.put(keysIter.next(), valuesIter.next());
            } else {
                map.put(keysIter.next(), null);
            }
        }
        return map;
    }

    public static <K, V> Tuple2<List<K>, List<V>> unzipMap(Map<K, V> map) {
        List<K> keys = arrayList();
        List<V> values = arrayList();
        map.forEach((k, v) -> {
            keys.add(k);
            values.add(v);
        });
        return new Tuple2<>(keys, values);
    }

    public static <T> List<T> concat(Iterable<T> list1, Iterable<T>... otherLists) {
        List<T> ret = new LinkedList<>();
        if (list1 != null) {
            for (T t : list1) {
                ret.add(t);
            }
        }
        if (otherLists != null) {
            for (Iterable<T> list : otherLists) {
                if (list != null) {
                    for (T t : list) {
                        ret.add(t);
                    }
                }
            }
        }
        return ret;
    }

    public static <K, V> Map<K, V> concat(Map<K, V> map1, Map<K, V>... others) {
        Map<K, V> map = hashMap();
        if (map1 != null) {
            map.putAll(map1);
        }
        if (others != null) {
            for (Map<K, V> it : others) {
                if (it != null) {
                    map.putAll(it);
                }
            }
        }
        return map;
    }

    public static <T> List<T> reverse(List<T> coll) {
        if (coll == null) {
            return Collections.emptyList();
        }
        List<T> newColl = empty(coll);
        newColl.addAll(coll);
        Collections.reverse(newColl);
        return newColl;
    }

    public static <K, V> Map<V, K> inverse(Map<K, V> map) {
        if (map == null) {
            return Collections.emptyMap();
        }
        //java使用擦除法泛型，所以这里可以转换成功
        Map<V, K> newMap = (Map<V, K>) empty(map);
        map.forEach((k, v) -> newMap.put(v, k));
        return newMap;
    }

    public static <T> List<T> slice(List<T> list, int startInclusion, int endInclusion) {
        int size = list.size();
        if (startInclusion < 0) {
            startInclusion += size;
        }
        if (endInclusion < 0) {
            endInclusion += size;
        }
        return list.subList(startInclusion, endInclusion + 1);
    }

    public static <T> Set<T> diff(Iterable<T> c1, Iterable<T> c2) {
        Set<T> set1 = castOrConvert(c1);
        Set<T> set2 = castOrConvert(c2);
        return Sets.difference(set1, set2);
    }

    public static <T> Set<T> intersect(Iterable<T> c1, Iterable<T> c2) {
        Set<T> set1 = castOrConvert(c1);
        Set<T> set2 = castOrConvert(c2);
        return Sets.intersection(set1, set2);
    }


    public static <T> Set<T> union(Iterable<T> c1, Iterable<T> c2) {
        Set<T> set1 = castOrConvert(c1);
        Set<T> set2 = castOrConvert(c2);
        return Sets.union(set1, set2);
    }

    private static <T> Set<T> castOrConvert(Iterable<T> iter) {
        if (iter instanceof Set) {
            return Objects.cast(iter);
        } else {
            Set<T> set = hashSet();
            for (T e : iter) {
                set.add(e);
            }
            return set;
        }
    }

    public static <T> void each(Iterable<T> iterable, BiConsumer<Integer, T> fn) {
        Iterator<T> iter = iterable.iterator();
        int idx = 0;
        while (iter.hasNext()) {
            fn.accept(idx++, iter.next());
        }
    }

    public static <T> void each(Iterable<T> iterable, Consumer<T> fn) {
        Iterator<T> iter = iterable.iterator();
        while (iter.hasNext()) {
            fn.accept(iter.next());
        }
    }

    public static <K, V> void each(Map<K, V> coll, BiConsumer<K, V> fn) {
        Iterator<Map.Entry<K, V>> iter = coll.entrySet().iterator();
        Map.Entry<K, V> e;
        while (iter.hasNext()) {
            e = iter.next();
            fn.accept(e.getKey(), e.getValue());
        }
    }

    public static <T> void each(Stream<T> stream, Consumer<T> fn) {
        stream.forEachOrdered(fn);
    }

    public static <E> List<E> remove(List<E> coll, Collection<E> toRemove) {
        Set<E> set = castOrConvert(toRemove);
        List<E> newColl = empty(coll);
        for (E e : coll) {
            if (!set.contains(e)) {
                newColl.add(e);
            }
        }
        return newColl;
    }

    public static <E> Set<E> remove(Set<E> coll, Collection<E> toRemove) {
        Set<E> set = castOrConvert(toRemove);
        Set<E> newColl = empty(coll);
        for (E e : coll) {
            if (!set.contains(e)) {
                newColl.add(e);
            }
        }
        return newColl;
    }

    public static <K, V> Map<K, V> remove(Map<K, V> coll, Collection<K> toRemove) {
        Set<K> set = castOrConvert(toRemove);
        Map<K, V> newColl = empty(coll);
        coll.forEach((k, v) -> {
            if (!set.contains(k)) {
                newColl.put(k, v);
            }
        });
        return newColl;
    }

    public static IntStream range(int endExclusive) {
        return IntStream.range(0, endExclusive);
    }

    public static IntStream range(int startInclusive, int endExclusive) {
        return IntStream.range(startInclusive, endExclusive);
    }

    public static <K, V> Map<K, List<V>> groupBy(List<V> coll, Function<V, K> fn) {
        return coll.stream().collect(Collectors.groupingBy(fn::apply));
    }

    public static <K> Map<K, List<K>> groupBy(Set<K> coll, Function<K, K> fn) {
        return coll.stream().collect(Collectors.groupingBy(fn::apply));
    }

    public static <T> T mapBean(Map<String, Object> map, Class<T> clazz) {
        BeanMap beanMap = BeanMap.create(ObjenesisHelper.newInstance(clazz));
        beanMap.putAll(map);
        return (T) beanMap.getBean();
    }

    public static BeanMap beanMap(Object obj) {
        return BeanMap.create(obj);
    }

    public static <T> T[] toArray(Collection<T> coll,Class<T> clazz){
        if(coll==null){
            return null;
        }
        return coll.toArray(Arrays.newArray(clazz,0));
    }

    public static <T> T getAt(List<T> list, int idx){
        if(idx<0){
            idx += list.size();
        }
        return list.get(idx);
    }
    public static <T> List<T> getAt(List<T> list, int... idxs){
        if(idxs == null){
            return list;
        }
        List<T> ret = new ArrayList<>();
        for(int idx : idxs){
            ret.add(getAt(list,idx));
        }
        return ret;
    }
}
