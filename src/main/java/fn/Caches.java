package fn;

import com.google.common.base.Suppliers;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static fn.Arrays.array;
import fn.Fns.*;

/**
 * 类似于mybatis的二级缓存。
 */
public abstract class Caches {
    //ignore-mixin-begin
    private Caches(){}
    //ignore-mixin-end
    public static final AtomicInteger cacheCounter = new AtomicInteger(0);
    public static final ThreadLocal<Map<String, Object>> cacheTl = new InheritableThreadLocal<>();

    /**
     * we can define lazyinit field
     */

    public static <T> Supplier<T> memoize(Supplier<T> supplier) {
        return Suppliers.memoize(supplier::get);
    }

    /**
     * we can define lazyinit field with Expiration
     * such as openapi, the token has a expire time.
     */

    public static <T> Supplier<T> memoizeWithExpiration(Supplier<T> supplier, long duration, TimeUnit unit) {
        return Suppliers.memoizeWithExpiration(supplier::get, duration, unit);
    }


    public static String parseExpression(String expression, Object args) {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("args", args);
        Expression exp = parser.parseExpression(expression);
        //取出解析结果
        return String.valueOf(exp.getValue(context));
    }
}
