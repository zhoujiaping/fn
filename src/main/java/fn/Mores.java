package fn;

import fn.tuple.Tuple2;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public abstract class Mores {
    //ignore-mixin-begin
    private Mores(){}
    //ignore-mixin-end
    public static <T> T todo(Object... args) {
        throw new RuntimeException("todo...");
    }

    public static <T, R> R also(T obj, Function<T, R> f) {
        return f.apply(obj);
    }

    public static <T> T let(T obj, Consumer<T> f) {
        f.accept(obj);
        return obj;
    }
    public static <T> Tuple2<Long,T> time(Supplier<T> fn){
        long startAt = System.currentTimeMillis();
        T res = fn.get();
        long finishAt = System.currentTimeMillis();
        return Tuples.tuple(finishAt-startAt,res);
    }
}
