package fn.tuple;

import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static fn.Colls.arrayList;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
@FieldDefaults(makeFinal = true)
public class Tuple4<V1, V2, V3, V4> implements Serializable ,Tuple{

    static final long serialVersionUID = 1L;

    public V1 _1;
    public V2 _2;
    public V3 _3;
    public V4 _4;

    private AtomicReference<List<Object>> itemsRef = new AtomicReference<>();

    @Override
    public int size() {
        return 4;
    }

    @Override
    public List<Object> items() {
        return arrayList(_1,_2,_3,_4);
    }
}
