package fn.tuple;

import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static fn.Colls.arrayList;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
@FieldDefaults(makeFinal = true)
public class Tuple6<V1, V2, V3, V4, V5, V6> implements Serializable,Tuple {

    static final long serialVersionUID = 1L;

    public V1 _1;
    public V2 _2;
    public V3 _3;
    public V4 _4;
    public V5 _5;
    public V6 _6;

    private AtomicReference<List<Object>> itemsRef = new AtomicReference<>();

    @Override
    public int size() {
        return 6;
    }

    @Override
    public List<Object> items() {
        return arrayList(_1,_2,_3,_4,_5,_6);
    }
}
