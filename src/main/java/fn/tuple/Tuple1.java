package fn.tuple;

import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static fn.Colls.arrayList;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
@FieldDefaults(makeFinal = true)
public class Tuple1<V1> implements Serializable ,Tuple{

    static final long serialVersionUID = 1L;

    public V1 _1;
    private AtomicReference<List<Object>> itemsRef = new AtomicReference<>();

    @Override
    public int size() {
        return 1;
    }

    @Override
    public List<Object> items() {
        return arrayList(_1);
    }
}
