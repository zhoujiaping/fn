package fn.tuple;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static fn.Colls.arrayList;

public class Tuple0 implements Serializable,Tuple {

    static final long serialVersionUID = 1L;
    private AtomicReference<List<Object>> itemsRef = new AtomicReference<>();

    @Override
    public int size() {
        return 0;
    }

    @Override
    public List<Object> items() {
        return arrayList();
    }
}
