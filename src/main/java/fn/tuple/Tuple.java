package fn.tuple;

import java.util.List;

public interface Tuple {
    int size();
    List<Object> items();

}
