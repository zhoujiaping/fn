package fn.tuple;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static fn.Colls.arrayList;


@ToString
@EqualsAndHashCode
@AllArgsConstructor
@FieldDefaults(makeFinal = true)
public class Tuple2<V1, V2> implements Serializable ,Tuple{

    static final long serialVersionUID = 1L;

    public V1 _1;
    public V2 _2;
    private AtomicReference<List<Object>> itemsRef = new AtomicReference<>();

    @Override
    public int size() {
        return 2;
    }

    @Override
    public List<Object> items() {
        return arrayList(_1,_2);
    }
}
