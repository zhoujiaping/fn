package fn;

import java.util.concurrent.*;

import com.google.common.base.Suppliers;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.Lombok;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Supplier;

import static fn.Colls.arrayList;

/**
 * java8并行流、CompletableFuture 都默认使用 ForkJoinPool的commonPool。
 * 它们的线程数为cpu核数，在cpu密集的场景可以充分利用cpu。
 * 但是在io密集的场景，则会因为io阻塞，浪费cpu资源，不能充分利用cpu。
 * 一般对于io密集的场景，线程数设置为cpu核数的两倍。
 * <p>
 * 对于定时任务，可以用单独的一个池，里面只有1个线程。
 * 如果是比较小的任务，当定时任务到达需要执行的时间，可以直接在定时任务的线程执行该任务。
 * 如果是比较大的任务，当定时任务到达需要执行的时间，建议将任务提交到其他线程池。定时任务线程，仅仅做定时的功能。
 * <p>
 * 如果需要并行计算，建议用java8的并行流。
 * 如果需要按顺序执行一系列任务（cpu密集型），可以构建一个集合（只有一个元素），创建并行流，并行流之后再用并行流。
 * 比如 arrayList(1).stream().parallel().map(a->a+1).map(a->a+2).map((a->a+3));
 * 这样就可以按顺序执行a+1，a+2，a+3的任务，但是我们不需要关注任务的线程分配。
 * <p>
 * 如果需要按顺序执行一系列任务（io密集型），可以创建一个CompletableFuture，设置线程池为我们定义的io线程池。
 * 然后添加任务。
 * <p>
 * 该类目标:
 * 简化定时任务相关的代码；
 * 简化多线程任务的代码；
 */
@Slf4j
public abstract class Threads {
    //ignore-mixin-begin
    private Threads(){}
    public static final Supplier<ScheduledThreadPoolExecutor> scheduledExecutorSupplier =
            Suppliers.memoize(() -> new ScheduledThreadPoolExecutor(1, factory(() -> "schedule-%s"),
                    new ThreadPoolExecutor.AbortPolicy() {
                    }));
    public static final int CORE_CPU_NUM = Runtime.getRuntime().availableProcessors();
    public static final Supplier<ExecutorService> executorSupplier = Suppliers.memoize(() ->
            new ThreadPoolExecutor(CORE_CPU_NUM, CORE_CPU_NUM * 2, 1, TimeUnit.DAYS, new LinkedBlockingDeque<>(), factory(() -> "thread-%s")));

    private static ThreadFactory factory(Supplier<String> nameFormatSupplier) {
        return new ThreadFactoryBuilder().setDaemon(true)
                .setNameFormat(nameFormatSupplier.get())
                .setUncaughtExceptionHandler((t, e) -> log.error("", e)).build();
    }

    //ignore-mixin-end
    public static ExecutorService commonThreadPool() {
        return ForkJoinPool.commonPool();
    }

    public static ExecutorService commonIoThreadPool() {
        return executorSupplier.get();
    }

    @SneakyThrows
    public static <T> T sleep(long millis) {
        Thread.sleep(millis);
        return null;
    }

    public static <T> T execute(Runnable fn) {
        executorSupplier.get().execute(fn);
        return null;
    }

    public static <T> Future<T> submit(Callable<T> fn) {
        return executorSupplier.get().submit(fn);
    }

    @SneakyThrows
    public static synchronized void shutdownThreadPool() {
        arrayList(executorSupplier, scheduledExecutorSupplier).forEach(supplier -> {
            ExecutorService executor = supplier.get();
            if (!executor.isShutdown()) {
                executor.shutdown();
            }
            boolean terminated;
            do {
                try {
                    terminated = executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    throw Lombok.sneakyThrow(e);
                }
            } while (!terminated);
        });
    }

    public static <V> ScheduledFuture<V> setTimeout(long delayMilliseconds, Runnable runnable) {
        return scheduledExecutorSupplier.get().schedule(
                () -> {
                    executorSupplier.get().execute(runnable);
                    return null;
                }, delayMilliseconds, TimeUnit.MILLISECONDS);
    }

    public static <V> ScheduledFuture<V> setTimeoutInScheduleThread(long delayMilliseconds, Callable<V> callable) {
        return scheduledExecutorSupplier.get().schedule(callable, delayMilliseconds, TimeUnit.MILLISECONDS);
    }

    public static ScheduledFuture<?> setInterval(long initialDelayMilliseconds,
                                                 long periodMilliseconds, Runnable command) {
        return scheduledExecutorSupplier.get().scheduleAtFixedRate(() -> executorSupplier.get().execute(command),
                initialDelayMilliseconds, periodMilliseconds, TimeUnit.MILLISECONDS);
    }

    public static ScheduledFuture<?> setIntervalInScheduleThread(long initialDelayMilliseconds,
                                                                 long periodMilliseconds, Runnable command) {
        return scheduledExecutorSupplier.get().scheduleAtFixedRate(command,
                initialDelayMilliseconds, periodMilliseconds, TimeUnit.MILLISECONDS);
    }
}

