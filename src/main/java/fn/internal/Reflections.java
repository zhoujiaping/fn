package fn.internal;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import fn.utils.Classs;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

public abstract class Reflections extends ReflectionUtils {

    private static final Method[] EMPTY_METHOD_ARRAY = new Method[0];

    private static final Map<Class<?>, Method[]> declaredMethodsCache;

    static {
        Field declaredMethodsCacheField = findField(ReflectionUtils.class, "declaredMethodsCache");
        makeAccessible(declaredMethodsCacheField);
        try {
            declaredMethodsCache = (Map<Class<?>, Method[]>) declaredMethodsCacheField.get(null);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * the orig method name is findMethod.
     * we changed it to responseTo,
     * and we changed implementation of hasSameParams
     */
    public static List<Method> responseTo(Class<?> clazz, String name, Object... args) {
        Assert.notNull(clazz, "Class must not be null");
        Assert.notNull(name, "Method name must not be null");
        Class<?> searchType = clazz;
        Class<?>[] paramTypes = null;
        if (args != null) {
            paramTypes = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                if (args[i] == null) {
                    paramTypes[i] = null;
                }
                paramTypes[i] = args[i].getClass();
            }
        }
        List<Method> foundMethods = new LinkedList<>();
        while (searchType != null) {
            Method[] methods = (searchType.isInterface() ? searchType.getMethods() :
                    getDeclaredMethods(searchType, false));
            for (Method method : methods) {
                if (name.equals(method.getName()) && (paramTypes == null || hasSameParams(method, paramTypes))) {
                    foundMethods.add(method);
                }
            }
            searchType = searchType.getSuperclass();
        }
        return foundMethods;
    }


    private static boolean hasSameParams(Method method, Class<?>[] paramTypes) {
        return typeMatches(paramTypes, method.getParameterTypes());
    }

    //and we changed the method typeMatches
    public static boolean typeMatches(Class[] actualParamTypes, Class[] virtualParamTypes) {
        if (actualParamTypes.length != virtualParamTypes.length) {
            return false;
        }
        int len = virtualParamTypes.length;
        for (int i = 0; i < len; i++) {
            Class act = actualParamTypes[i];
            Class vir = virtualParamTypes[i];
            //if actual argument is null, we treat it as Any type
            if (act == null) {
                continue;
            }
            //if virtual argument is primitive, we use it's wrapper class
            if (vir.isPrimitive()) {
                vir = Classs.getWrapperClass(vir);
            }
            if (!vir.isAssignableFrom(act)) {
                return false;
            }
        }
        return true;
    }

    private static Method[] getDeclaredMethods(Class<?> clazz, boolean defensive) {
        Assert.notNull(clazz, "Class must not be null");
        Method[] result = declaredMethodsCache.get(clazz);
        if (result == null) {
            try {
                Method[] declaredMethods = clazz.getDeclaredMethods();
                List<Method> defaultMethods = findConcreteMethodsOnInterfaces(clazz);
                if (defaultMethods != null) {
                    result = new Method[declaredMethods.length + defaultMethods.size()];
                    System.arraycopy(declaredMethods, 0, result, 0, declaredMethods.length);
                    int index = declaredMethods.length;
                    for (Method defaultMethod : defaultMethods) {
                        result[index] = defaultMethod;
                        index++;
                    }
                } else {
                    result = declaredMethods;
                }
                declaredMethodsCache.put(clazz, (result.length == 0 ? EMPTY_METHOD_ARRAY : result));
            } catch (Throwable ex) {
                throw new IllegalStateException("Failed to introspect Class [" + clazz.getName() +
                        "] from ClassLoader [" + clazz.getClassLoader() + "]", ex);
            }
        }
        return (result.length == 0 || !defensive) ? result : result.clone();
    }


    private static List<Method> findConcreteMethodsOnInterfaces(Class<?> clazz) {
        List<Method> result = null;
        for (Class<?> ifc : clazz.getInterfaces()) {
            for (Method ifcMethod : ifc.getMethods()) {
                if (!Modifier.isAbstract(ifcMethod.getModifiers())) {
                    if (result == null) {
                        result = new ArrayList<>();
                    }
                    result.add(ifcMethod);
                }
            }
        }
        return result;
    }

}
