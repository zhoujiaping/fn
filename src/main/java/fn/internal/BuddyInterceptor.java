package fn.internal;

import fn.utils.Methods;
import lombok.SneakyThrows;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.implementation.bind.annotation.AllArguments;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperMethod;
import net.bytebuddy.implementation.bind.annotation.This;
import org.apache.commons.lang3.mutable.MutableObject;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

/**
 * intercept class
 */
@Slf4j
public abstract class BuddyInterceptor {
    public static final String HANDLER_FIELD_NAME = "$buddy$h";
    //public static final String SUPERCLASS_NAME_METHOD_NAME = "_15f078f4_2f32_413d_93b8_30251c9219f7";

    /**
     * 生成代理类。可代理接口和实现类。可代理静态方法和final方法。
     * 注意：代理静态方法的话，要通过反射来调用。
     * 虽然代理静态方法、final方法是不好的实践。但是有的场景，别人提供的代码设计上有缺陷，或者在一些非正式场合（比如测试代码），还是有用武之地的。
     * @param targetClass
     * @param handler
     * @param <T>
     * @return
     */
    public static <T> Class<? extends T> proxy(Class<T> targetClass, ProxyHandler handler) {
        return proxy(targetClass, handler,null);
    }

    /**
     * 生成代理类。可代理接口和实现类。
     * @param targetClass
     * @param filter
     * @param handler
     * @param <T>
     * @return
     */
    @SneakyThrows
    public static <T> Class<? extends T> proxy(Class<T> targetClass, ProxyHandler handler, Predicate<Method> filter) {
        Objects.requireNonNull(targetClass, "targetClass can't be null");
        if (ClassUtils.isPrimitiveOrWrapper(targetClass)) {
            throw new RuntimeException("can't proxy a primitive or wrapper class");
        }
        Predicate<Method> methodFilter;
        if (filter == null) {
            methodFilter = method -> !Methods.isObjectDefaultOrOverride(method);
        } else {
            methodFilter = method -> !Methods.isObjectDefaultOrOverride(method) && filter.test(method);
        }
        DynamicType.Builder<T> builder = new ByteBuddy().subclass(targetClass);
        MutableObject<DynamicType.Builder<T>> builderHolder = new MutableObject<>(builder);
        List<Method> proxyMethods = new LinkedList<>();
        ReflectionUtils.doWithMethods(targetClass, method -> {
            //将方法委托给BuddyInterceptor#intercept。intercept中会再委托给handlers中对应的ProxyHandler。
                    builderHolder.setValue(
                            builderHolder.getValue().define(method).intercept(MethodDelegation.to(BuddyInterceptor.class))
                    );
                    proxyMethods.add(method);
                },
                method -> methodFilter.test(method));
        log.debug("proxyMethods={}", proxyMethods);
        Class<? extends T> generated = builderHolder.getValue()/*.defineMethod(SUPERCLASS_NAME_METHOD_NAME, String.class,
                Modifier.PRIVATE | Modifier.STATIC | Modifier.FINAL).intercept(
                FixedValue.value(targetClass.getName()))*/
                .defineField(HANDLER_FIELD_NAME,ProxyHandler.class, Modifier.PRIVATE|Modifier.STATIC)
                .make()
                .load(Thread.currentThread().getContextClassLoader())
                .getLoaded();
        setHandler(generated, handler);
        return generated;
    }

    //@Synchronized
    @SneakyThrows
    public static void setHandler(Class<?> clazz, ProxyHandler handler) {
        Field h = clazz.getDeclaredField(HANDLER_FIELD_NAME);
        if(!h.isAccessible()){
            h.setAccessible(true);
        }
        h.set(null,handler);
    }
    //@Synchronized
    @SneakyThrows
    public static ProxyHandler getHandler(Class<?> clazz) {
        Field h = clazz.getDeclaredField(HANDLER_FIELD_NAME);
        if(!h.isAccessible()){
            h.setAccessible(true);
        }
        return (ProxyHandler) h.get(null);
    }

    /**
     * bytebuddy生成的代理类的代理方法
     *
     * @param args
     * @param target
     * @param method
     * @param origClass
     * @param superMethod
     * @return
     */
    @SneakyThrows
    @RuntimeType
    public static Object intercept(@AllArguments Object[] args,
                                   @This(optional = true) Object target,
                                   @Origin Method method,
                                   @Origin Class<?> origClass,
                                   @SuperMethod(nullIfImpossible = true) Method superMethod) {
        //if it is static method, the superMethod will be null. we need find it's superMethod
        if (superMethod == null) {
            superMethod = ReflectionUtils.findMethod(origClass, method.getName(), method.getParameterTypes());
        }
        return getHandler(method.getDeclaringClass()).invoke(target, method, superMethod, args);
    }

}
