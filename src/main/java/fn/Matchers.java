package fn;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Matchers {
    private static Map<String, Pattern> patternCache = new ConcurrentHashMap<>();

    //ignore-mixin-begin
    private Matchers(){}
    //ignore-mixin-end
    /**
     * 正则匹配，返回匹配结果的迭代器
     * */
    public static Iterator<MatchResult> match(Matcher matcher) {
        int[] start = {0};
        return new Iterator<MatchResult>() {
            @Override
            public boolean hasNext() {
                return matcher.find(start[0]);
            }

            @Override
            public MatchResult next() {
                MatchResult r = matcher.toMatchResult();
                start[0] = matcher.end();
                return r;
            }
        };
    }

    /**
     * 正则替换
     * */
    public static String replaceAll(String str, String reg, BiFunction<Integer, String, String> fn) {
        Pattern p = pattern(reg);
        Matcher matcher = p.matcher(str);
        StringBuilder sb = new StringBuilder();
        int nth = 0;
        int start = 0;
        int end = 0;
        String group;
        while (matcher.find(start)) {
            end = matcher.end();
            group = matcher.group();
            sb.append(str, start, end - group.length()).append(fn.apply(nth++, group));
            start = end;
        }
        sb.append(str, end, str.length());
        return sb.toString();
    }

    /**
     * 替换前n个。n从0开始算。
     */
    public static String replaceFirstN(String str, String reg, int n, BiFunction<Integer, String, String> fn) {
        Pattern p = pattern(reg);
        Matcher matcher = p.matcher(str);
        StringBuilder sb = new StringBuilder();
        int nth = 0;
        int start = 0, end = 0;
        String group;
        while (matcher.find(start)) {
            end = matcher.end();
            group = matcher.group();
            sb.append(str, start, end - group.length());
            if (nth <= n) {
                sb.append(fn.apply(nth, group));
            } else if (nth == n+1) {
                sb.append(group);
                break;
            }
            start = end;
            nth++;
        }
        sb.append(str, end, str.length());
        return sb.toString();
    }

    /**
     * 正则替换。替换第n个匹配的
     * @param str
     * @param reg
     * @param n
     * @param fn
     * @return
     */
    public static String replaceNth(String str,String reg,int n,BiFunction<Integer, String, String> fn){
        return replaceFirstN(str,reg,n,(idx,s)->idx==n?fn.apply(idx,s):s);
    }

    /**
     * 编译正则得到pattern，会将pattern缓存起来。
     * @param reg
     * @return
     */
    public static Pattern pattern(String reg) {
        Pattern pattern = patternCache.computeIfAbsent(reg, r->Pattern.compile(r));
        if (pattern == null) {
            //虽然这里不是原子操作，但是没关系，最多是多编译一次或多次正则
            pattern = Pattern.compile(reg);
            patternCache.put(reg, pattern);
        }
        return pattern;
    }

    /**
     * 正则匹配
     * */
    public static boolean match(String str, String reg) {
        return pattern(reg).matcher(str).find();
    }

}
