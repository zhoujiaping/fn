package fn;

import fn.tuple.*;

public abstract class Tuples {
    //ignore-mixin-begin
    private Tuples(){}
    //ignore-mixin-end

    public static Tuple0 tuple() {
        return new Tuple0();
    }


    public static <V1> Tuple1<V1> tuple(V1 _1) {
        return new Tuple1<>(_1);
    }


    public static <V1, V2> Tuple2<V1, V2> tuple(V1 _1, V2 _2) {
        return new Tuple2<>(_1, _2);
    }


    public static <V1, V2, V3> Tuple3<V1, V2, V3> tuple(V1 _1, V2 _2, V3 _3) {
        return new Tuple3<>(_1, _2, _3);
    }


    public static <V1, V2, V3, V4> Tuple4<V1, V2, V3, V4> tuple(V1 _1, V2 _2, V3 _3, V4 _4) {
        return new Tuple4<>(_1, _2, _3, _4);
    }


    public static <V1, V2, V3, V4, V5> Tuple5<V1, V2, V3, V4, V5> tuple(V1 _1, V2 _2, V3 _3, V4 _4, V5 _5) {
        return new Tuple5<>(_1, _2, _3, _4, _5);
    }


    public static <V1, V2, V3, V4, V5, V6> Tuple6<V1, V2, V3, V4, V5, V6> tuple(V1 _1, V2 _2, V3 _3, V4 _4, V5 _5, V6 _6) {
        return new Tuple6<>(_1, _2, _3, _4, _5, _6);
    }


    public static <V1, V2, V3, V4, V5, V6, V7> Tuple7<V1, V2, V3, V4, V5, V6, V7> tuple(V1 _1, V2 _2, V3 _3, V4 _4, V5 _5, V6 _6, V7 _7) {
        return new Tuple7<>(_1, _2, _3, _4, _5, _6, _7);
    }

}
